﻿/api
AuthController

POST /auth/user/signup 

{
  "hoTen": "HoTen",
  "email": "example@gmail.com",
  "soDienThoai": "0123445678",
  "password": "sago"
}

Return 
{
  "statusCode": 1,
  "message": "Dang ky thanh cong"
}
{
  "statusCode": -1,
  "message": "email da ton tai"
}

POST /auth/user/signin 

{
  "email": "example@gmail.com",
  "password": "password"
}

Return
{
  "statusCode": 1,
  "message": "Đăng nhập thành công",
  "token": "token"
}
{
  "statusCode": -1,
  "message": "Đăng nhập thất bại"
}


POST /auth/staff/signin 

{
  "username": "example",
  "password": "password"
}

Return
{
  "statusCode": 1,
  "message": "Đăng nhập thành công",
  "token": "token"
}

GET/auth/sendemail
{
  "email": "example@gmail.com"
}

Return
{
  "statusCode": 1,
  "message": "Gửi email thành công"
}



UserController /users
GET 

Return
{"createdAt":"2020-11-06T06:56:54.000+00:00","updatedAt":"2020-11-06T06:56:54.000+00:00","id":6,"hoTen":"Phan Công Sơn","email":"phancongha28@gmail.com","soDienThoai":"0929315514","wishList":[],"notificationList":[],"verifiedEmail":false,"locked":false},{"createdAt":"2020-11-06T07:03:38.000+00:00","updatedAt":"2020-11-06T07:03:38.000+00:00","id":7,"hoTen":"Phan Công Sơn","email":"phancongha29@gmail.com","soDienThoai":"0929315514","wishList":[],"notificationList":[],"verifiedEmail":false,"locked":false}

POST /changepassword
{
  "password": "newpassword"
}

Return
{
  "statusCode": 1,
  "message": "Đã cập nhật mật khẩu mới"
}

GET /recoverypassword
{
  "email": "example@gmail.com"
}

Return
{
  "statusCode": -1,
  "message": "Email đã tồn tại"
}
{
  "statusCode": 1,
  "message": "Mật khẩu mới đã được gửi vào email"
}

GET /profile
{
  "id": 8
}

Return
{
  "hoTen": "Phan Công Sơn",
  "soDienThoai": "0929315514",
  "email": "phancongha30@gmail.com",
  "danhSachBaiDang": [
    {
      "id": 11,
      "tieuDe": "Kí túc xá/Homestay Bình Quới, Quận Bình Thạnh",
      "ngayDang": "2020-11-13T09:44:17.000+00:00",
      "trangThai": "Approved",
      "nhanVienDuyet": "Hệ Thống",
      "postDetail": {
        "id": 4,
        "gia": 1500000,
        "datCoc": 1500000,
        "giaTienDien": 1500000,
        "giaTienNuoc": 1500000,
        "giaTienWifi": 1500000,
        "dienTich": "40",
        "sucChua": "5-10",
        "trangThaiPhong": false,
        "diaChi": "117/2b Bình Quới, Phường 27, Quận Bình Thạnh, Hồ Chí Minh",
        "chuoiTienIch": "tv-ad",
        "moTa": "KTX_tân_bình  - Giờ giấc tự do - Phòng chỉ 4 người\n---------------------\n Ktx tân bình tọa lạc tại vị trí trung tâm thuận tiện đi lại các quận: Q1, Q3, Phú Nhuận, Gò Vấp\n  Cạnh các khu vực:\n CV Hoàng Văn Thụ, Lotte Mart Cộng Hoà, ADORA, AUCHAN, Sân bay Tân Sơn Nhất, ...\n Trường Saigontouris, CĐ Lý Tự Trọng, ĐH Văn Hiến, Đh Bách Khoa, ĐH Kinh Tế, CĐ Hàng Không\n---------------------\n Phòng rộng thoáng mát, có máy lạnh, có ban công, giường, rèm, nệm, kệ sách, tủ cá nhân.\n Nhà để xe có camera 24/24, \n Phòng giặt: Mát giặt, xào phơi đồ.\n Khu bếp: Tủ lạnh, bếp điện, bàn ăn, kệ chén,...\n---------------------\n Gía chỉ 1tr5/người/tháng\n----------------------\nLiên hệ trực tiếp ktx tân bình để nhận thêm ưu đãi\n Zalo: 0832.232.232\n 214/10 Hoàng Văn Thụ, Phường 4, Q. Tân Bình. (Đối diện ADORA)",
        "soDienThoai": null
      }
    }
  ]
}

POST /profile

Return 
{
  "hoTen": "Phan Công Hà",
  "email": "phancongha24@gmail.com",
  "soDienThoai": "0929315514",
  "xacThucEmail": true,
  "danhSachBaiDang": [
    {
      "id": 1,
      "tieuDe": "Căn hộ dịch vụ Tôn Thất Tùng, Quận 1",
      "ngayDang": "2020-11-08T13:36:46.000+00:00",
      "trangThai": "Pending",
      "nhanVienDuyet": "Hệ Thống",
      "postDetail": {
        "id": 5,
        "gia": 1500000,
        "datCoc": 1500000,
        "giaTienDien": 1500000,
        "giaTienNuoc": 1500000,
        "giaTienWifi": 1500000,
        "dienTich": "40",
        "sucChua": "5-10",
        "trangThaiPhong": false,
        "diaChi": "25/40 B Tôn Thất Tùng, Phường Phạm Ngũ Lão, Quận 1, Hồ Chí Minh",
        "chuoiTienIch": "tv-ad",
        "moTa": "An ninh, trật tự, sạch sẽ và yên tĩnh. Mặt tiền đường ô tô, gần nhiều chợ, siêu thị mini, thuận tiện đến các quận trung tâm!!",
        "soDienThoai": "0932735616"
      }
    },
    {
      "id": 2,
      "tieuDe": "Kí túc xá/Homestay Hoàng Văn Thụ, Quận Tân Bình",
      "ngayDang": "2020-11-08T13:37:17.000+00:00",
      "trangThai": "Pending",
      "nhanVienDuyet": "Hệ Thống",
      "postDetail": {
        "id": 6,
        "gia": 1500000,
        "datCoc": 1500000,
        "giaTienDien": 1500000,
        "giaTienNuoc": 1500000,
        "giaTienWifi": 1500000,
        "dienTich": "40",
        "sucChua": "5-10",
        "trangThaiPhong": false,
        "diaChi": "214/10 Hoàng Văn Thụ, Phường 4, Quận Tân Bình, Hồ Chí Minh",
        "chuoiTienIch": "ml-nvsr-bdx-wf",
        "moTa": "KTX_tân_bình  - Giờ giấc tự do - Phòng chỉ 4 người\n---------------------\n Ktx tân bình tọa lạc tại vị trí trung tâm thuận tiện đi lại các quận: Q1, Q3, Phú Nhuận, Gò Vấp\n  Cạnh các khu vực:\n CV Hoàng Văn Thụ, Lotte Mart Cộng Hoà, ADORA, AUCHAN, Sân bay Tân Sơn Nhất, ...\n Trường Saigontouris, CĐ Lý Tự Trọng, ĐH Văn Hiến, Đh Bách Khoa, ĐH Kinh Tế, CĐ Hàng Không\n---------------------\n Phòng rộng thoáng mát, có máy lạnh, có ban công, giường, rèm, nệm, kệ sách, tủ cá nhân.\n Nhà để xe có camera 24/24, \n Phòng giặt: Mát giặt, xào phơi đồ.\n Khu bếp: Tủ lạnh, bếp điện, bàn ăn, kệ chén,...\n---------------------\n Gía chỉ 1tr5/người/tháng\n----------------------\nLiên hệ trực tiếp ktx tân bình để nhận thêm ưu đãi\n Zalo: 0832.232.232\n 214/10 Hoàng Văn Thụ, Phường 4, Q. Tân Bình. (Đối diện ADORA)",
        "soDienThoai": "0832232232"
      }
    },
    {
      "id": 3,
      "tieuDe": "CHDV Sân Bay TSN. Full Nội Thất Mức Giá Thuê Cực Rẻ",
      "ngayDang": "2020-11-08T13:43:19.000+00:00",
      "trangThai": "Pending",
      "nhanVienDuyet": "Hệ Thống",
      "postDetail": {
        "id": 7,
        "gia": 1500000,
        "datCoc": 5500000,
        "giaTienDien": 1500000,
        "giaTienNuoc": 100000,
        "giaTienWifi": 100000,
        "dienTich": "40",
        "sucChua": "5-10",
        "trangThaiPhong": false,
        "diaChi": "2R Trường Sơn, Phường 2, Quận Tân Bình, Hồ Chí Minh",
        "chuoiTienIch": "nvsr-bdx-wf",
        "moTa": "Địa chỉ: 2R Trường Sơn, P2, Tân Bình\nMặt tiền, khu sân bay cao cấp.\nNgay lá phổi Xanh CV Hoàng Văn Thụ\n\n Free phí dịch vụ: Máy giặt + Sấy, rác, vệ sinh nhà hàng tuần, hút bụi tận phòng, để xe máy...\n\n Nội thất Full cao cấp: Sofa, giường tủ quần áo, tủ kệ bếp, bàn ăn, Smart tivi 40”, Máy lạnh, tủ lạnh, Bếp hồng ngoại cao cấp...\n\n Giá thuê: \n- CHDV 28m2, giá thuê 6.5 triệu -> Ưu đãi tháng đầu tiên 5.5 Triệu/ tháng ( Cửa sổ, logia)\n\n Quản lý chuyên nghiệp theo hệ thống, luôn có người bảo trì, vận hành thân thiện\n\n Xem phòng: 097.379.7286 Mr. Hùng\n)",
        "soDienThoai": "0973797286"
      }
    }
  ]
}


WishListController /wishlist
POST /mywishlist
{
  "myWishList": [
    {
      "id": 2,
      "tieuDe": "Kí túc xá/Homestay Hoàng Văn Thụ, Quận Tân Bình",
      "ngayDang": "2020-11-08T13:37:17.000+00:00",
      "trangThai": "Pending",
      "nhanVienDuyet": "Hệ Thống",
      "postDetail": {
        "id": 6,
        "gia": 1500000,
        "datCoc": 1500000,
        "giaTienDien": 1500000,
        "giaTienNuoc": 1500000,
        "giaTienWifi": 1500000,
        "dienTich": "40",
        "sucChua": "5-10",
        "trangThaiPhong": false,
        "diaChi": "214/10 Hoàng Văn Thụ, Phường 4, Quận Tân Bình, Hồ Chí Minh",
        "chuoiTienIch": "ml-nvsr-bdx-wf",
        "moTa": "KTX_tân_bình  - Giờ giấc tự do - Phòng chỉ 4 người\n---------------------\n Ktx tân bình tọa lạc tại vị trí trung tâm thuận tiện đi lại các quận: Q1, Q3, Phú Nhuận, Gò Vấp\n  Cạnh các khu vực:\n CV Hoàng Văn Thụ, Lotte Mart Cộng Hoà, ADORA, AUCHAN, Sân bay Tân Sơn Nhất, ...\n Trường Saigontouris, CĐ Lý Tự Trọng, ĐH Văn Hiến, Đh Bách Khoa, ĐH Kinh Tế, CĐ Hàng Không\n---------------------\n Phòng rộng thoáng mát, có máy lạnh, có ban công, giường, rèm, nệm, kệ sách, tủ cá nhân.\n Nhà để xe có camera 24/24, \n Phòng giặt: Mát giặt, xào phơi đồ.\n Khu bếp: Tủ lạnh, bếp điện, bàn ăn, kệ chén,...\n---------------------\n Gía chỉ 1tr5/người/tháng\n----------------------\nLiên hệ trực tiếp ktx tân bình để nhận thêm ưu đãi\n Zalo: 0832.232.232\n 214/10 Hoàng Văn Thụ, Phường 4, Q. Tân Bình. (Đối diện ADORA)",
        "soDienThoai": "0832232232"
      }
    }
  ]
}
POST /add/postID
{
  "statusCode": 1,
  "message": "Đã thêm vào danh sách thích"
}
POST /delete/postID
{
  "statusCode": 1,
  "message": "Đã xóa khỏi danh sách"
}



StaffController /staffs

POST 
{
  "hoTen": "HoTen",
  "username": "example",
  "soDienThoai": "012345678",
  "luong": 1999999,
  "roleType": 1
}

Return
{
  "statusCode": 0,
  "message": "Username đã tồn tại"
}
{
  "statusCode": 1,
  "message": "Tạo nhân viên thành công"
}
{
  "statusCode": -1,
  "message": "Bạn không có quyền truy cập"
}

GET

Return
[
  {
    "createdAt": "2020-11-08T13:33:53.000+00:00",
    "updatedAt": "2020-11-08T13:33:53.000+00:00",
    "id": 0,
    "hoTen": "Hệ Thống",
    "username": "system",
    "soDienThoai": "",
    "namSinh": "",
    "luong": null
  }
]

POST /aproving
{
  "postID": 11,
  "trangThai": "Approved",
  "liDo": ""
}

Return
{
  "statusCode": 1,
  "message": "Cập nhật thành công"
}
{
  "statusCode": -1,
  "message": "Bạn không có quyền truy cập"
}

POST /ban/userID
{
  "statusCode": 1,
  "message": "Khóa user thành công"
}
{
  "statusCode": -1,
  "message": "Bạn không có quyền truy cập"
}

POST /unban/userID
{
  "statusCode": 1,
  "message": "Mở khóa user thành công"
}
{
  "statusCode": -1,
  "message": "Bạn không có quyền truy cập"
}
