package com.sago.SagoStay.Service;

import com.sago.SagoStay.Model.Notification;

public interface ExpoNotificationService {
    void sendMessage(String title,Notification notifcation);
}
