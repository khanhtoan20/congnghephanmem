package com.sago.SagoStay.Repository;

import com.sago.SagoStay.Model.ExpoNotification;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ExpoNotificationRepository extends JpaRepository<ExpoNotification,Long> {
    ExpoNotification findByUserId(Long userID);

}
