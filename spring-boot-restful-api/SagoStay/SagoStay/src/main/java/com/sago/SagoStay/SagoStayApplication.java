package com.sago.SagoStay;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.sago.SagoStay.Configuration.FileStorageConfig;
import com.sago.SagoStay.Model.Post;
import com.sago.SagoStay.Payload.PostRequest;
import com.sago.SagoStay.Security.JWTAuthenticationFilter;
import lombok.SneakyThrows;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.core.convert.converter.Converter;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.TimeZone;

@SpringBootApplication
@EnableJpaAuditing
@EnableConfigurationProperties(FileStorageConfig.class)
public class SagoStayApplication {

	public static void main(String[] args) {
		SpringApplication.run(SagoStayApplication.class, args);
	}
	@PostConstruct
	void init() {
		TimeZone.setDefault(TimeZone.getTimeZone("Etc/UTC"));
	}
	@Bean
	public JWTAuthenticationFilter JWTAuthenticationFilter() {
		return new JWTAuthenticationFilter();
	}
	@Component
	public static class StringToUserConverter implements Converter<String, PostRequest> {

		@Autowired
		private ObjectMapper objectMapper;

		@SneakyThrows
		public PostRequest convert(String source) {
			return objectMapper.readValue(source, PostRequest.class);
		}
	}
}
