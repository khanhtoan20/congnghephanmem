package com.sago.SagoStay.Model;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.RequiredArgsConstructor;
import org.hibernate.annotations.CreationTimestamp;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.Calendar;
import java.util.Date;


@Entity
@Data
@NoArgsConstructor
public class VerificationToken {
    private static final int Expired = 60*24;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @ManyToOne(targetEntity = User.class)
    @JoinColumn(name="user_id",nullable = false)
    private  User user;
    @Column(unique = true)
    private  String token;

    private Date expiredDate;

    public VerificationToken(User User,String Token){
        this.user =User;
        this.token = Token;
    }

}
