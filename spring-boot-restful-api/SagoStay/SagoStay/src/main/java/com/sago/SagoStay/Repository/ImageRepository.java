package com.sago.SagoStay.Repository;

import com.sago.SagoStay.Model.Image;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ImageRepository extends JpaRepository<Image,Long> {
    void deleteAllByPostId(Long postID);
}
