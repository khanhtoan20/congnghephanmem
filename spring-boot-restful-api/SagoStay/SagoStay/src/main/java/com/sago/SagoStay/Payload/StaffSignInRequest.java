package com.sago.SagoStay.Payload;

import lombok.Data;

import javax.validation.constraints.NotBlank;

@Data
public class StaffSignInRequest {
    @NotBlank
    private String username;
    @NotBlank
    private String password;
}
