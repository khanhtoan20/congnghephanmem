package com.sago.SagoStay.Repository;

import com.sago.SagoStay.Model.Facility;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface FacilityRepository extends JpaRepository<Facility,String> {
    boolean existsByMaTienIch(String maTienIch);
}
