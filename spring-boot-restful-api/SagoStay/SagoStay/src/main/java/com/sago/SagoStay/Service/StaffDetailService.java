package com.sago.SagoStay.Service;

import com.sago.SagoStay.Security.StaffDetail;

public interface StaffDetailService {
    StaffDetail loadStaffById(Long ID);
}
