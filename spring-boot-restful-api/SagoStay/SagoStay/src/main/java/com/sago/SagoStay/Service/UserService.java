package com.sago.SagoStay.Service;

import com.sago.SagoStay.Model.Notification;
import com.sago.SagoStay.Model.User;
import com.sago.SagoStay.Model.VerificationToken;
import com.sago.SagoStay.Payload.ExpoToken;
import com.sago.SagoStay.Payload.UserResponse;

import javax.servlet.http.HttpServletRequest;
import javax.transaction.Transactional;
import java.util.List;

public interface UserService {
    void save(User user);
    User getUser(String token);
    void createToken(User user, String token);
    VerificationToken getToken(String token);
    boolean existsByEmail(String email);
    User findByEmail(String email);
    void deleteAllToken(User user);
    void updateProfile(Long id, String phoneNumber);
    User findById(Long id);
    User JWTokenToUser(HttpServletRequest httpServletRequest);
    String generateRecoveryPassword(int length);
    List<User> getAllUser();
    List<UserResponse> getAllBannedUser();
    List<UserResponse> getAllUnbannedUser();
    List<Notification> getAllNotifications(Long userID);
    void updateExpoToken(Long userID,ExpoToken expoToken);
}
