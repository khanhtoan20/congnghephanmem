package com.sago.SagoStay.Payload;

import lombok.Data;

import javax.servlet.http.HttpServletRequest;
import javax.validation.constraints.NotBlank;
@Data
public class UserUpdateRequest{
    @NotBlank
    private String phoneNumber;
}
