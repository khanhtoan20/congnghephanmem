package com.sago.SagoStay.Model;


import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.Embedded;
import java.io.Serializable;
@Embeddable
public class WishListID implements Serializable {
    @Column(name = "user_id")
    private Long userId;
    @Column(name = "post_id")
    private Long postId;
}
