package com.sago.SagoStay.Service.Impl;

import com.google.firebase.messaging.FirebaseMessaging;
import com.google.firebase.messaging.FirebaseMessagingException;
import com.google.firebase.messaging.Message;
import com.sago.SagoStay.Payload.PnsRequest;
import com.sago.SagoStay.Service.FCMService;
import org.springframework.stereotype.Service;

@Service
public class FCMServiceImpl implements FCMService {
    @Override
    public String pushNotification(PnsRequest pnsRequest) {
        Message message = Message.builder()
                .putData("content", pnsRequest.getContent())
                .setToken(pnsRequest.getFcmToken())
                .build();

        String response = null;
        try {
            response = FirebaseMessaging.getInstance().send(message);
            return response;
        } catch (FirebaseMessagingException e) {
            e.printStackTrace();
        }
        return null;
    }
    }

