package com.sago.SagoStay.Repository;

import com.sago.SagoStay.Model.Notification;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface NotificationRepository extends JpaRepository<Notification,Long> {
    List<Notification> findByUserIdOrderByCreatedAtDesc(Long userID);
    List<Notification> findByUserIdAndStatusOrderByCreatedAtDesc(Long userID,Boolean isRead);
}
