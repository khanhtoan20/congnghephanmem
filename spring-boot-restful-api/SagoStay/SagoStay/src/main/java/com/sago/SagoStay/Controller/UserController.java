package com.sago.SagoStay.Controller;

import com.sago.SagoStay.Model.Staff;
import com.sago.SagoStay.Model.User;
import com.sago.SagoStay.Payload.*;
import com.sago.SagoStay.Security.JWTokenProvider;
import com.sago.SagoStay.Security.StaffDetail;
import com.sago.SagoStay.Security.UserDetail;
import com.sago.SagoStay.Service.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import javax.mail.Multipart;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/api/users")
public class UserController {
    @Autowired
    private NotificationService notificationService;
    @Autowired
    private PasswordEncoder passwordEncoder;
    @Autowired
    private UserService userService;
    @Autowired
    private StaffService staffService;
    @Autowired
    private JWTokenProvider JWTokenProvider;
    @Autowired
    private UserDetailService userDetailService;
    @Autowired
    private PostService postService;
    @Autowired
    private EmailService emailService;
    @Autowired
    private FileStorageService fileStorageService;

    @PostMapping("/update")
    public ResponseEntity<BaseResponseEntity> updateProfile(@Valid @RequestBody UserUpdateRequest userUpdateRequest, @AuthenticationPrincipal UserDetail userDetail) {
        userService.updateProfile(userDetail.getId(), userUpdateRequest.getPhoneNumber());
        return new ResponseEntity<>(new BaseResponseEntity(1, "Cập nhật thành công"), HttpStatus.OK);
    }
    @GetMapping("/profile/{id}")
    public ResponseEntity<BaseResponseEntity> getUserProfile(@PathVariable(name = "id") Long id)
    {
        User user = userService.findById(id);
        if(user!= null)
        return new ResponseEntity(new UserProfileResponse(user.getHoTen(),user.getSoDienThoai(),user.getEmail() ,postService.getAllApprovedPostsOfUser(user.getId())),HttpStatus.OK);
        return new ResponseEntity(new BaseResponseEntity(-1,"Id không tồn tại"),HttpStatus.OK);
    }
    @PostMapping("/profile")
    public ResponseEntity getMyProfile(HttpServletRequest request, @AuthenticationPrincipal UserDetail userDetail)
    {
        User user = userService.findById(userDetail.getId());
        return new ResponseEntity(new MyProfileResponse(user.getHoTen(),user.getEmail(),user.getSoDienThoai(),user.isVerifiedEmail(),notificationService.countUnreadNotification(user.getId()),user.getExpoNotification().isTurnOn()),HttpStatus.OK);
    }
    @PostMapping("/notification")
    public ResponseEntity getNotifcations(@AuthenticationPrincipal UserDetail userDetail)
    {
        User user = userService.findById(userDetail.getId());
        return new ResponseEntity(userService.getAllNotifications(userDetail.getId()),HttpStatus.OK);
    }
    @PostMapping("/recoverypassword")
    public ResponseEntity recoveryPassword(@Valid @RequestBody recoveryPasswordRequest recoveryPasswordRequest, HttpServletRequest request)
    {
        User user = userService.findByEmail(recoveryPasswordRequest.getEmail());
        if(user == null)
        {
            return new ResponseEntity<>(new BaseResponseEntity(-1, "Email không tồn tại"), HttpStatus.NOT_FOUND);
        }
        try {
        String newPassword = userService.generateRecoveryPassword(8);
        user.setPassword(passwordEncoder.encode(newPassword));
        userService.save(user);
        new Thread(()->{
            emailService.recoveryPasswordEmail(user.getEmail(),newPassword);
        }).start();
            return new ResponseEntity<>(new BaseResponseEntity(1, "Mật khẩu mới đã được gửi vào email: "+user.getEmail()), HttpStatus.OK);
        } catch (Exception ex) {
            return ResponseEntity.badRequest().body(null);
        }
    }
    @PostMapping("/expo")
    public void updateExpoToken(@RequestBody ExpoToken expoToken,@AuthenticationPrincipal UserDetail userDetail){
        userService.updateExpoToken(userDetail.getId(),expoToken);
    }
    @PostMapping("/changepassword")
    public ResponseEntity changePassword(@Valid @RequestBody changePasswordRequest changePasswordRequest,@AuthenticationPrincipal UserDetail userDetail)
    {
        User user = userService.findById(userDetail.getId());
        String newPassword = changePasswordRequest.getNewPassword();
        if(!passwordEncoder.matches(changePasswordRequest.getOldPassword(),user.getPassword()))
            return new ResponseEntity<>(new BaseResponseEntity(-1, "Sai mật khẩu cũ"), HttpStatus.OK);
        user.setPassword(passwordEncoder.encode(newPassword));
        userService.save(user);
        return new ResponseEntity<>(new BaseResponseEntity(1, "Đã cập nhật mật khẩu mới"), HttpStatus.OK);
    }

//    @PostMapping("/logout")
//    public ResponseEntity logout(HttpServletRequest request)
//    {
//        User user = userService.JWTokenToUser(request);
//        userService.deleteAllToken(user);
//        return new ResponseEntity<>(new BaseResponseEntity(1, "Đăng xuất thành công"), HttpStatus.OK);
//    }

    @GetMapping("/image/{fileName}")
    public ResponseEntity getImage(@PathVariable String fileName,HttpServletRequest request){
        try {
        Resource resource = fileStorageService.loadFileAsResource(fileName);

            return ResponseEntity.ok().contentType(MediaType.IMAGE_PNG).body(Files.readAllBytes(resource.getFile().toPath()));
        }catch(Exception ex){
            return ResponseEntity.noContent().build();
        }
    }
    @GetMapping
    public ResponseEntity getAllUser()
    {
        return new ResponseEntity(userService.getAllUser(),HttpStatus.OK);
    }
    @GetMapping("/banned")
    public ResponseEntity<List<UserResponse>> getAllBannedUser()
    {
        return new ResponseEntity(userService.getAllBannedUser(),HttpStatus.OK);
    }
    @GetMapping("/unbanned")
    public ResponseEntity<List<UserResponse>> getAllUnbannedUser()
    {
        return new ResponseEntity(userService.getAllUnbannedUser(),HttpStatus.OK);
    }
}
