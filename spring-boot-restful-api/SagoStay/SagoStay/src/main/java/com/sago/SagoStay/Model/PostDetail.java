package com.sago.SagoStay.Model;

import lombok.Data;
import lombok.NoArgsConstructor;
import com.fasterxml.jackson.annotation.JsonIgnore;
import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import java.io.Serializable;

@Entity
@Data
@NoArgsConstructor
public class PostDetail implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="id")
    private Long id;

    @NotBlank
    @JsonIgnore
    @OneToOne
    @JoinColumn(name="post_id")
    private Post post;

    @NotBlank
    @Column(name="gia")
    private String gia;

     @NotBlank
    @Column(name="datcoc")
    private String datCoc;
    @NotBlank
    @Column(name="giatiendien")
    private String giaTienDien;
    @NotBlank
    @Column(name="giatiennuoc")
    private String giaTienNuoc;
    @NotBlank
    @Column(name="giatienwifi")
    private String giaTienWifi;
    @NotBlank
    @Column(name="dientich")
    private String dienTich;
    @NotBlank
    @Column(name="succhua")
    private String sucChua;
    @NotBlank
    @Column(name="trangthaiphong")
    private boolean trangThaiPhong;
    @NotBlank
    @Column(name="diachi")
    private String diaChi;
    @NotBlank
    @Column(name="chuoitienich")
    private String chuoiTienIch;
    @Column(name="mota",columnDefinition = "LONGTEXT")
    private String moTa;
    @Column(name="lienlac")
    private String soDienThoai;

    @JsonIgnore
    public Post getPost(){
        return this.post;
    }

    public PostDetail(PostDetail postDetail){
        this.post = postDetail.getPost();
        this.gia = postDetail.getGia();
        this.datCoc = postDetail.datCoc;
        this.giaTienDien = postDetail.getGiaTienDien();
        this.giaTienNuoc = postDetail.getGiaTienNuoc();
        this.giaTienWifi = postDetail.getGiaTienWifi();
        this.dienTich = postDetail.getDienTich();
        this.sucChua = postDetail.getSucChua();
        this.trangThaiPhong = postDetail.isTrangThaiPhong();
        this.diaChi =postDetail.getDiaChi();
        this.chuoiTienIch =postDetail.getChuoiTienIch();
        this.moTa = postDetail.getMoTa();
        this.soDienThoai = postDetail.getSoDienThoai();
    }
}
