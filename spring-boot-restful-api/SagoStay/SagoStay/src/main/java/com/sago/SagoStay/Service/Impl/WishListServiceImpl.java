package com.sago.SagoStay.Service.Impl;

import com.sago.SagoStay.Model.*;
import com.sago.SagoStay.Payload.PostResponse;
import com.sago.SagoStay.Repository.PostRepository;
import com.sago.SagoStay.Repository.UserRepository;
import com.sago.SagoStay.Repository.WishListRepository;

import com.sago.SagoStay.Service.PostService;
import com.sago.SagoStay.Service.WishListService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
@Service
@Transactional
public class WishListServiceImpl implements WishListService {
    @Autowired
    private WishListRepository wishListRepository;
    @Autowired
    private PostRepository postRepository;
    @Override
    public void save(WishList wishList) {
        wishListRepository.save(wishList);
    }
    @Override
    public void delete(WishList wishList) {
        wishListRepository.delete(wishList);
    }

    @Override
    /**
     * findWishList()
     * find WishList base on userId and postId
     * @param userId Long - to find user's List<WishList>
     * @param postId Long - to find user's List<WishList>
     * @return WishList
     */
    public WishList findWishList(Long userId, Long postId)
    {
        List<WishList> wishList = wishListRepository.findByUserId(userId);
        for(WishList wish : wishList)
        {
            if ( wish.getPostId() == postId)
            {
                return wish;
            }
        }
        return null;
    }

    @Override
    /**
     * getMyWishList()
     * Transform List<WishList> into List<Post> base on postId [List<WishList> find by userId]
     * @param id Long - stand for userId to get user's List<WishList>
     * @return List<Post>
     */
    public List<Post> getMyWishList(Long id)
    {
        List<WishList> myWishList = wishListRepository.findByUserId(id);
        List<Post> postList = new ArrayList<>();
        for (WishList wish : myWishList)
        {
            Post post = postRepository.getOne(wish.getPostId());
            postList.add(post);
        }
        return postList;
    }
}
