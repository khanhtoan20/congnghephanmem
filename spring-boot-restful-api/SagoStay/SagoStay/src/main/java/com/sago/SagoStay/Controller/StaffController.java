package com.sago.SagoStay.Controller;

import com.sago.SagoStay.Model.*;
import com.sago.SagoStay.Payload.*;
import com.sago.SagoStay.Security.StaffDetail;
import com.sago.SagoStay.Security.UserDetail;
import com.sago.SagoStay.Service.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cache.annotation.AbstractCachingConfiguration;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.*;

import javax.persistence.Access;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;

@RequestMapping("api/staffs")
@RestController
public class StaffController {
    @Value("${app.defaultpassword}")
    private String defaultPassword;
    @Autowired
    private StaffService staffService;
    @Autowired
    private UserService userService;
    @Autowired
    private RoleService roleService;
    @Autowired
    private PasswordEncoder passwordEncoder;
    @Autowired
    private PostService postService;
    @Autowired
    private ReportService reportService;

    @PostMapping
    @PreAuthorize("hasAuthority('2') or hasAuthority('1')")
    public ResponseEntity<BaseResponseEntity> createStaff(@Valid @RequestBody StaffSignUpRequest staffSignUpRequest)
    {
        try {
            if(staffService.existsByUsername(staffSignUpRequest.getUsername()))
                 return new ResponseEntity<>(new BaseResponseEntity(0, "Username đã tồn tại"), HttpStatus.OK);

            Role role = roleService.findRoleById(staffSignUpRequest.getRoleType());
            Staff staff = new Staff(
                    staffSignUpRequest.getHoTen(),
                    staffSignUpRequest.getUsername(),
                    passwordEncoder.encode(defaultPassword),staffSignUpRequest.getSoDienThoai(),staffSignUpRequest.getNamSinh(),staffSignUpRequest.getLuong(),role);
            staffService.save(staff);
            return new ResponseEntity<>(new BaseResponseEntity(1, "Tạo nhân viên thành công"), HttpStatus.OK);
        } catch (AccessDeniedException ex) {
            return new ResponseEntity<>(new BaseResponseEntity(-1, "Bạn không có quyền truy cập"), HttpStatus.OK);
        }
    }

    /**
     * updateStaff()
     * update Staff by postId
     * @param staffId Long - stand for ID of staff that need to update
     * @param staffUpdateRequest HttpServletRequest - stand for request send form client
     * @return WishListResponse
     */
    @PostMapping("/update/{staffId}")
    @PreAuthorize("hasAuthority('2') or hasAuthority('1')")
    public ResponseEntity<BaseResponseEntity> updateStaff(@PathVariable(name = "staffId") Long staffId,@Valid @RequestBody StaffUpdateRequest staffUpdateRequest)
    {
        try {
            Role role = roleService.findRoleById(staffUpdateRequest.getRoleType());
            Staff staff = staffService.findById(staffId);
            staff.setHoTen(staffUpdateRequest.getHoTen());
            staff.setUsername(staffUpdateRequest.getUsername());
            staff.setSoDienThoai(staffUpdateRequest.getSoDienThoai());
            staff.setNamSinh(staffUpdateRequest.getNamSinh());
            staff.setLuong(staffUpdateRequest.getLuong());
            staff.setRole(role);
            staffService.save(staff);
            return new ResponseEntity<>(new BaseResponseEntity(1, "Cập nhật nhân viên thành công"), HttpStatus.OK);
        } catch (AccessDeniedException ex) {
            return new ResponseEntity<>(new BaseResponseEntity(403, "Bạn không có quyền truy cập"), HttpStatus.OK);
        }
    }

    @GetMapping
    @PreAuthorize("hasAuthority('1') or hasAuthority('5')")
    public ResponseEntity getAllStaff()
    {
        try{
            return new ResponseEntity(staffService.getAllStaffResponse(),HttpStatus.OK);
        }catch(AccessDeniedException ex){
            return new ResponseEntity<>(new BaseResponseEntity(403, "Bạn không có quyền truy cập"), HttpStatus.OK);
        }
    }

    @PostMapping("/approving")
    public ResponseEntity approvedPost(@Valid @RequestBody ApprovePostRequest approvePostRequest, @AuthenticationPrincipal StaffDetail staffDetail)
    {
        try{
            History history = new History(postService.getPostById(
                    approvePostRequest.getPostID()),
                    staffService.findById(staffDetail.getId()),
                    approvePostRequest.getTrangThai(),
                    approvePostRequest.getLiDo());
            staffService.approvingPost(history);
            return new ResponseEntity(new BaseResponseEntity(1,"Cập nhật thành công"),HttpStatus.OK);
        }catch(AccessDeniedException ex){
            return new ResponseEntity<>(new BaseResponseEntity(-1, "Bạn không có quyền truy cập"), HttpStatus.OK);
        }
    }

    @PostMapping("/handling/{reportId}")
    @PreAuthorize("hasAuthority('1') or hasAuthority('4')")
    public ResponseEntity<BaseResponseEntity> handleReport(@PathVariable(name = "reportId") Long reportId, @AuthenticationPrincipal StaffDetail staffDetail)
    {
        try {
            Report report = reportService.findById(reportId);
            report.setStatus("Handled");
            staffService.handleReport(report);
            return new ResponseEntity<>(new BaseResponseEntity(1, "Xử lí báo cáo hoàn tất"), HttpStatus.OK);
        } catch (AccessDeniedException ex) {
            return new ResponseEntity<>(new BaseResponseEntity(-1, "Bạn không có quyền truy cập"), HttpStatus.OK);
        }
    }
    @PostMapping("/ban/{userID}")
    @PreAuthorize("hasAuthority('1') or hasAuthority('5')")
    public ResponseEntity banUser(@PathVariable(name = "userID") Long userID,@AuthenticationPrincipal StaffDetail staffDetail)
    {
        try {
            staffService.banUser(userID);
            return new ResponseEntity(new BaseResponseEntity(1, "Khóa user thành công"), HttpStatus.OK);
        } catch (AccessDeniedException ex) {
            return new ResponseEntity<>(new BaseResponseEntity(-1, "Bạn không có quyền truy cập"), HttpStatus.OK);
        }
    }

    @PostMapping("/unban/{userID}")
    @PreAuthorize("hasAuthority('1') or hasAuthority('5')")
    public ResponseEntity unbanUser(@PathVariable(name = "userID") Long userID,@AuthenticationPrincipal StaffDetail staffDetail)
    {
        try {
            staffService.unbanUser(userID);
            return new ResponseEntity(new BaseResponseEntity(1, "Mở khhóa user thành công"), HttpStatus.OK);
        } catch (AccessDeniedException ex) {
            return new ResponseEntity<>(new BaseResponseEntity(-1, "Bạn không có quyền truy cập"), HttpStatus.OK);

        }
    }
    @PostMapping("notifyall")
    @PreAuthorize("hasAuthority('1') or hasAuthority('4')")
    public ResponseEntity notifyToAll(@Valid @RequestBody NotifyAllRequest notifyAllRequest)
    {
        try {
            staffService.notifyAll(notifyAllRequest);
            return new ResponseEntity(new BaseResponseEntity(1, "Thông báo thành công"), HttpStatus.OK);
        } catch (AccessDeniedException ex) {
            return new ResponseEntity<>(new BaseResponseEntity(-1, "Thông báo thất bại"), HttpStatus.OK);
        }
    }
}