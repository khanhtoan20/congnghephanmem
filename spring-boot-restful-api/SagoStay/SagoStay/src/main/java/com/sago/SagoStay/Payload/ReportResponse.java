package com.sago.SagoStay.Payload;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.sago.SagoStay.Model.PostDetail;
import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.Date;
import java.util.List;

@Data
@AllArgsConstructor
public class ReportResponse {
    @JsonProperty
    private Long id;
    @JsonProperty
    private String reason;
    @JsonProperty
    private Long postId;
    @JsonProperty
    private Long userId;
    @JsonProperty
    private String status;
    @JsonProperty
    private Date createAt;
}