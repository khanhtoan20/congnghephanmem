package com.sago.SagoStay.Service;


import com.sago.SagoStay.Model.History;

public interface HistoryService {
    void save(History history);
    History getLastestStatus(Long id);
}
