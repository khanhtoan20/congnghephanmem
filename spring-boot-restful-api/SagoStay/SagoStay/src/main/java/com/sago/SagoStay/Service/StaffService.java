package com.sago.SagoStay.Service;

import com.sago.SagoStay.Model.History;
import com.sago.SagoStay.Model.Report;
import com.sago.SagoStay.Model.Staff;
import com.sago.SagoStay.Payload.NotifyAllRequest;
import com.sago.SagoStay.Payload.StaffResponse;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

public interface StaffService {
    void save(Staff Staff);
    Staff getStaff(String token);
    boolean existsByUsername(String username);
    Staff findByUsername(String email);
    void updateSoDienThoai(Long id, String soDienThoai);
    Staff findById(Long id);
    Staff JWTokenToStaff(HttpServletRequest httpServletRequest);
    Staff getSystemStaff();
    List<Staff> getAllStaff();
    List<StaffResponse> getAllStaffResponse();
    void approvingPost(History history);
    void handleReport(Report report);
    void banUser(Long userID);
    void unbanUser(Long userID);
    void notifyAll(NotifyAllRequest notifyAllRequest);
}
