package com.sago.SagoStay.Payload;

import lombok.Data;

import javax.validation.constraints.NotBlank;
@Data
public class recoveryPasswordRequest {
    @NotBlank
    private String email;
}
