package com.sago.SagoStay.Service.Impl;

import com.sago.SagoStay.Model.Notification;
import com.sago.SagoStay.Repository.NotificationRepository;
import com.sago.SagoStay.Service.NotificationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class NotificationServiceImpl implements NotificationService {
    @Autowired
    private NotificationRepository notificationRepository;
    @Override
    public void save(Notification notification) {
        notificationRepository.save(notification);

    }

    @Override
    public List<Notification> getAllNotificationsByUserId(Long userID) {
        return notificationRepository.findByUserIdOrderByCreatedAtDesc(userID);
    }

    public String countUnreadNotification(Long userID){
        return notificationRepository.findByUserIdAndStatusOrderByCreatedAtDesc(userID,false).size()+"";
    }
}
