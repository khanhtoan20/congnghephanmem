package com.sago.SagoStay.Repository;

import com.sago.SagoStay.Model.User;
import com.sago.SagoStay.Model.VerificationToken;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface VerificationTokenRepository extends JpaRepository<VerificationToken,Long> {
    VerificationToken findByToken(String token);
    VerificationToken findByUser(User User);
    void deleteByUser(User User);
}
