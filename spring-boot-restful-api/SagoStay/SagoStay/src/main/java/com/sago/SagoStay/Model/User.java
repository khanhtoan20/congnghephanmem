package com.sago.SagoStay.Model;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import lombok.*;
import org.hibernate.annotations.NaturalId;

import javax.persistence.*;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;


@Entity
@NoArgsConstructor
@Data
@Table(name="users",uniqueConstraints ={@UniqueConstraint(columnNames = {"email"})})
public class User extends DateEntity{
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @NotBlank
    @Column(name = "hoten")

    private String hoTen;

    @JsonIgnore
    @NotBlank
    @Size(max = 100)
    @Column(name = "password")
    private String password;

    @NotBlank
    @NaturalId
    @Size(max = 40)
    @Column(name = "email")
    @Email
    private String email;

    @Column(name = "sodienthoai")
    private String soDienThoai;

    @Column(name = "is_verified_email")
    private boolean isVerifiedEmail;

    @Column(name ="is_locked")
    private boolean isLocked;
    @JsonIgnore
    @OneToMany(fetch = FetchType.LAZY,mappedBy = "user" )
    private List<VerificationToken> tokenList = new ArrayList<>();

    @OneToMany (fetch = FetchType.LAZY,mappedBy = "userId")
    private List<WishList> wishList = new ArrayList<>();

    @OneToMany (fetch = FetchType.LAZY,mappedBy = "user")
    private List<Notification> notificationList = new ArrayList<>();

    @JsonIgnore
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "user")

    private List<Post> postList = new ArrayList<>();
    @JsonIgnore
    @OneToOne(mappedBy = "user")
    private ExpoNotification expoNotification;
    public User(String hoTen,String soDienThoai, String email,String password,boolean isVerifiedEmail,boolean isLocked){
        this.email = email;
        this.hoTen = hoTen;
        this.soDienThoai = soDienThoai;
        this.password = password;
        this.isVerifiedEmail = isVerifiedEmail;
        this.isLocked = isLocked;
    }

}
