package com.sago.SagoStay.Payload;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.sago.SagoStay.Model.Facility;
import com.sago.SagoStay.Model.PostDetail;
import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.Date;
import java.util.List;

@Data
@AllArgsConstructor
public class PostResponse {
    @JsonProperty
    private Long id;
    @JsonProperty
    private String tieuDe;
    @JsonProperty
    private Date ngayDang;
    @JsonProperty
    private String trangThai;
    @JsonProperty
    private String nhanVienDuyet;
    @JsonProperty
    private String status;
    @JsonProperty
    private PostDetail postDetail;
    @JsonProperty
    private List<Facility> facilities;
    @JsonProperty
    private List<String> images;
    @JsonProperty
    private Boolean isAuthor;
    public PostResponse(Long id,String tieuDe,Date ngayDang,String trangThai, String nhanVienDuyet,String status,PostDetail postDetail,List<Facility> facilities,List<String> images){
        this.id=id;
        this.tieuDe=tieuDe;
        this.ngayDang=ngayDang;
        this.trangThai=trangThai;
        this.nhanVienDuyet=nhanVienDuyet;
        this.status=status;
        this.postDetail=postDetail;
        this.facilities=facilities;
        this.images=images;
    }
}
