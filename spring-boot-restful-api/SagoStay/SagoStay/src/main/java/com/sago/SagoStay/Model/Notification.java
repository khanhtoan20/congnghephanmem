package com.sago.SagoStay.Model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;

@Entity
@NoArgsConstructor
@Data
@Table(name="notification",uniqueConstraints ={@UniqueConstraint(columnNames = {"id"})})
public class Notification extends DateEntity{
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @NotBlank
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "user_id")
    @JsonIgnore
    private User user;

    @Column(name = "content")
    private String content;

    @Column(name = "type")
    private int type;

    @Column(name = "status")
    private Boolean status;

    public Notification(User user,String content,int type,Boolean status){
        this.user = user;
        this.content = content;
        this.type = type;
        this.status =status;
    }
}
