package com.sago.SagoStay.Model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;

@Entity
@NoArgsConstructor
@Data
@Table(name="report",uniqueConstraints ={@UniqueConstraint(columnNames = {"id"})})
public class Report extends DateEntity{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @NotBlank
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "post_id")
    @JsonIgnore
    private Post post;

    @NotBlank
    @OneToOne
    @JoinColumn(name = "user_id")
    @JsonIgnore
    private User user;

    @NotBlank
    @Column(name = "reason")
    private String reason;

    @Column(name = "status")
    private String status;

    public Report(Post post, User user, String reason){
        this.post = post;
        this.user = user;
        this.reason = reason;
        this.status = "Pending";
    }
    public Report(Post post, User user, String reason,String status){
        this.post = post;
        this.user = user;
        this.reason = reason;
        this.status = status;
    }
}
