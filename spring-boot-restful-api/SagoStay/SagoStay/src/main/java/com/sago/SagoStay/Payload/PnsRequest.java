package com.sago.SagoStay.Payload;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
public class PnsRequest {
    private String fcmToken;
    private String content;

}
