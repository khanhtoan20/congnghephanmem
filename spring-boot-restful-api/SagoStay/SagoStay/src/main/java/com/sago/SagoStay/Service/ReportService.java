package com.sago.SagoStay.Service;

import com.sago.SagoStay.Model.Post;
import com.sago.SagoStay.Model.Report;
import com.sago.SagoStay.Payload.ReportResponse;

import java.util.List;

public interface ReportService {
    void save(Report report);
    Report findById(Long id);
    List<Report> getAllReport();
    ReportResponse getPendingReportResponse(Report report);
    List<ReportResponse> getAllPendingReportResponse(List<Report> reports);
    List<ReportResponse> getAllPendingReports();
}
