package com.sago.SagoStay.Service.Impl;

import com.sago.SagoStay.Model.*;
import com.sago.SagoStay.Payload.ExpoToken;
import com.sago.SagoStay.Payload.PostResponse;
import com.sago.SagoStay.Payload.UserProfileResponse;
import com.sago.SagoStay.Payload.UserResponse;
import com.sago.SagoStay.Repository.ExpoNotificationRepository;
import com.sago.SagoStay.Repository.NotificationRepository;
import com.sago.SagoStay.Repository.UserRepository;
import com.sago.SagoStay.Repository.VerificationTokenRepository;
import com.sago.SagoStay.Security.JWTokenProvider;
import com.sago.SagoStay.Service.NotificationService;
import com.sago.SagoStay.Service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service
@Transactional
public class UserServiceImpl  implements UserService {
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private NotificationService notificationService;
    @Autowired
    private VerificationTokenRepository verificationTokenRepository;
    @Autowired
    private JWTokenProvider jwTokenProvider;
    @Autowired
    private ExpoNotificationRepository expoNotificationRepository;
    public String generateRecoveryPassword(int length)
    {
        String AlphaNumericString = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
                + "0123456789"
                + "abcdefghijklmnopqrstuvxyz";
        StringBuilder sb = new StringBuilder(length);
        for (int i = 0; i < length; i++) {
            int index = (int)(AlphaNumericString.length() * Math.random());
            sb.append(AlphaNumericString.charAt(index));
        }
        return sb.toString();
    }
    @Override
    public void save(User user) {
        userRepository.save(user);
    }

    @Override
    public User getUser(String token) {
        return null;
    }

    @Override
    public void createToken(User user, String token) {
        verificationTokenRepository.save(new VerificationToken(user,token));
    }
    @Override
    public User findByEmail(String email){
        User user = userRepository.findByEmail(email);
        return user;
    }
    @Override
    public boolean existsByEmail(String Email){
        return userRepository.existsByEmail(Email);

    }

    @Override
    public void deleteAllToken(User User){
        verificationTokenRepository.deleteByUser(User);
    }


    @Override
    public VerificationToken getToken(String token) {
        return verificationTokenRepository.findByToken(token);
    }

    @Override
    public void updateProfile(Long id, String phoneNumber) {
        User user = findById(id);
        user.setSoDienThoai(phoneNumber);
        userRepository.save(user);
    }

    @Override
    public User findById(Long id) {
        User user = userRepository.findById(id).orElseThrow(()-> new UsernameNotFoundException(String.format("%s not found",id)));
        return user;
    }

    @Override
    public User JWTokenToUser(HttpServletRequest httpServletRequest) {
        String Token = httpServletRequest.getHeader("Authorization").substring(7);
        return userRepository.findById(jwTokenProvider.getIDByJWT(Token)).orElseThrow(()-> new UsernameNotFoundException("not found"));
    }

    public List<User> getAllUser()
    {
        return userRepository.findAll();
    }
    public List<UserResponse> getAllBannedUserResponse(List<User> users)
    {
        List<UserResponse> userResponseList = new ArrayList<>(users.size());
        for (User user : users)
        {
            UserResponse response = getBannedUserResponse(user);
            if (response!=null){
                userResponseList.add(response);
            }
        }
        return userResponseList;
    }
    public List<UserResponse> getAllUnbannedUserResponse(List<User> users)
    {
        List<UserResponse> userResponseList = new ArrayList<>(users.size());
        for (User user : users)
        {
            UserResponse response = getUnbannedUserResponse(user);
            if (response!=null){
                userResponseList.add(response);
            }
        }
        return userResponseList;
    }
    public UserResponse getBannedUserResponse(User user)
    {
        if(user.isLocked()){
            return new UserResponse(user.getId(),user.getHoTen(),user.getEmail());
        }
        return null;
    }
    public UserResponse getUnbannedUserResponse(User user)
    {
        if(!user.isLocked())
        {
            return new UserResponse(user.getId(),user.getHoTen(),user.getEmail());
        }
        return null;
    }
    @Override
    public List<UserResponse> getAllBannedUser()
    {
        return getAllBannedUserResponse(userRepository.findAll());
    }
    @Override
    public List<UserResponse> getAllUnbannedUser()
    {
        return getAllUnbannedUserResponse(userRepository.findAll());
    }
    @Override
    public List<Notification> getAllNotifications(Long userId)
    {
        return notificationService.getAllNotificationsByUserId(userId);
    }

    @Override
    public void updateExpoToken(Long userID, ExpoToken expoToken) {
        ExpoNotification expoNotification = expoNotificationRepository.findByUserId(userID);
        if(expoNotification==null)
            expoNotification = new ExpoNotification();
        expoNotification.setExpoToken(expoToken.getToken());
        expoNotification.setTurnOn(expoToken.getIsTurnOn());
        expoNotification.setUser(userRepository.findById(userID).get());
        expoNotificationRepository.save(expoNotification);
    }


}
