package com.sago.SagoStay.Security;

import com.sago.SagoStay.Service.StaffDetailService;
import com.sago.SagoStay.Service.UserDetailService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource;
import org.springframework.util.StringUtils;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Slf4j
public class JWTAuthenticationFilter extends OncePerRequestFilter {
    @Autowired
    private JWTokenProvider jwTokenProvider;
    @Autowired
    private UserDetailService userDetailService;
    @Autowired
    private StaffDetailService staffDetailService;

    @Override
    protected void doFilterInternal(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, FilterChain filterChain) throws ServletException, IOException {
       try{
           String Token = getJWTToken(httpServletRequest);
           if(StringUtils.hasText(Token) && jwTokenProvider.validateToken(Token)) {
               Long userID = jwTokenProvider.getIDByJWT(Token);
               UsernamePasswordAuthenticationToken authenticationToken;
               if (! jwTokenProvider.isAdmin(Token)) {
                   UserDetail userDetail = userDetailService.loadUserById(userID);
                   authenticationToken = new UsernamePasswordAuthenticationToken(userDetail, null,
                           userDetail.getAuthorities());

               }
               else{
                   StaffDetail staffDetail = staffDetailService.loadStaffById(userID);
                   authenticationToken = new UsernamePasswordAuthenticationToken(staffDetail,null,staffDetail.getAuthorities());
               }
               authenticationToken.setDetails(new WebAuthenticationDetailsSource().buildDetails(httpServletRequest));
               SecurityContextHolder.getContext().setAuthentication(authenticationToken);
           }
       } catch(Exception ex){
           log.error("failed on set authentication", ex);
       }
       filterChain.doFilter(httpServletRequest,httpServletResponse);
    }

    private  String getJWTToken(HttpServletRequest httpServletRequest) {
        String bearerToken = httpServletRequest.getHeader("Authorization");
        if (StringUtils.hasText(bearerToken) && bearerToken.startsWith("Bearer "))
            return bearerToken.substring(7);

        return null;
    }
}
