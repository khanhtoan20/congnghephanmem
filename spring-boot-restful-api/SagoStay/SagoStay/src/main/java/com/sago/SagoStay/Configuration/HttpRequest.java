package com.sago.SagoStay.Configuration;

import org.apache.http.client.methods.HttpPost;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.io.Closeable;
import java.net.HttpURLConnection;
import java.net.URL;

@Configuration
public class HttpRequest {
    @Bean
    public HttpPost post() {

        return new HttpPost("https://exp.host/--/api/v2/push/send");

    }

}