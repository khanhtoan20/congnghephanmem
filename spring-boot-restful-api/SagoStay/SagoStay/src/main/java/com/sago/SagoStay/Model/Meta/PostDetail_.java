package com.sago.SagoStay.Model.Meta;

import com.sago.SagoStay.Model.PostDetail;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(PostDetail.class)
public class PostDetail_ {
    public static volatile SingularAttribute<PostDetail, String> chuoiTienIch;
    public static volatile SingularAttribute<PostDetail, Long> dienTich;
    public static volatile SingularAttribute<PostDetail, Long > gia;
    public static volatile SingularAttribute<PostDetail, String > diaChi;

    public static final String CHUOITIENICH = "chuoiTienIch";
    public static final String DIENTICH = "dienTich";
    public static final String GIA = "gia";
    public static final String DIACHI = "diaChi";
}
