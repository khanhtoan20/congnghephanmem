package com.sago.SagoStay.Controller;

import com.sago.SagoStay.Model.Post;
import com.sago.SagoStay.Model.Staff;
import com.sago.SagoStay.Model.User;
import com.sago.SagoStay.Model.WishList;
import com.sago.SagoStay.Payload.BaseResponseEntity;
import com.sago.SagoStay.Payload.PostResponse;
import com.sago.SagoStay.Payload.WishListResponse;
import com.sago.SagoStay.Security.StaffDetail;
import com.sago.SagoStay.Security.UserDetail;
import com.sago.SagoStay.Service.PostService;
import com.sago.SagoStay.Service.UserService;
import com.sago.SagoStay.Service.WishListService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

@RestController
@RequestMapping("/api/wishlist")
public class WishListController {
    @Autowired
    private UserService userService;
    @Autowired
    private PostService postService;
    @Autowired
    private WishListService wishListService;
    /**
     * getMyWishList()
     * Get user List<WishList> by userId
     * @param userDetail UserDetail - contain User attributes get by Token in sending request
     * @return WishListResponse
     */
    @PostMapping("/mywishlist")
    public ResponseEntity<BaseResponseEntity> getMyWishList(@AuthenticationPrincipal UserDetail userDetail) {
        List<Post> postList = wishListService.getMyWishList(userDetail.getId());
        List<PostResponse> wishList = postService.getAllPostResponse(postList);
        return new ResponseEntity(new WishListResponse(wishList), HttpStatus.OK);
    }
    /**
     * add()
     * add a WishList which user like by postId
     * @param postId Long - stand for ID of post which user like
     * @param request HttpServletRequest - stand for request send form client
     * @param userDetail UserDetail - contain User attributes get by Token in sending request
     * @return WishListResponse
     */
    @PostMapping("/add/{postId}")
    public ResponseEntity<BaseResponseEntity> add(@PathVariable(name = "postId") Long postId,HttpServletRequest request, @AuthenticationPrincipal UserDetail userDetail){
        WishList wishList = new WishList(userDetail.getId(),postId);

        wishListService.save(wishList);
        return new ResponseEntity(new BaseResponseEntity(1,"Đã thêm vào danh sách thích"), HttpStatus.OK);
    }
    /**
     * delete()
     * delete a WishList base on userId and postId
     * @param postId Long - stand for ID of post which user like
     * @param request HttpServletRequest - stand for request send form client
     * @param userDetail UserDetail - contain User attributes get by Token in sending request
     * @return WishListResponse
     */
    @PostMapping("/delete/{postId}")
    public ResponseEntity<BaseResponseEntity> delete(@PathVariable(name = "postId") Long postId,HttpServletRequest request, @AuthenticationPrincipal UserDetail userDetail){
        WishList wish = wishListService.findWishList(userDetail.getId(),postId);

        wishListService.delete(wish);
        return new ResponseEntity(new BaseResponseEntity(1,"Đã xóa khỏi danh sách thích"), HttpStatus.OK);
    }

}
