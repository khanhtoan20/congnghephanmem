package com.sago.SagoStay.Repository;

import com.sago.SagoStay.Model.Staff;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import javax.swing.text.html.Option;
import java.util.Optional;

@Repository
public interface StaffRepository extends JpaRepository<Staff,Long> {
    boolean existsByUsername(String username);
    Staff findByUsername(String username);
    Staff findStaffById(Long id);
}
