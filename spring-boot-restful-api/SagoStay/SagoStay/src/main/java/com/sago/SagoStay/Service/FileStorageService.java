package com.sago.SagoStay.Service;

import org.springframework.web.multipart.MultipartFile;
import org.springframework.core.io.Resource;

public interface FileStorageService {
    String storeFile(MultipartFile multipartFile);
    Resource loadFileAsResource(String fileName);
}
