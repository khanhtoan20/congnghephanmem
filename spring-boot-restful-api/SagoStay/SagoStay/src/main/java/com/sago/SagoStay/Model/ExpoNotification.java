package com.sago.SagoStay.Model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Data
@Entity
@AllArgsConstructor
@NoArgsConstructor
public class ExpoNotification {
    @Id
    @GeneratedValue(strategy =GenerationType.IDENTITY)
    private Long Id;
    @OneToOne
    @JoinColumn(name="user_id")
    private User user;
    @Column(name="expotoken")
    private String expoToken;
    @Column(name="isturnon")
    private boolean isTurnOn;

}
