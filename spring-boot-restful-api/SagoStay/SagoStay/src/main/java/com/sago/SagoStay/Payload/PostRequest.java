package com.sago.SagoStay.Payload;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.sago.SagoStay.Model.PostDetail;
import com.sun.istack.Nullable;
import lombok.AllArgsConstructor;
import lombok.Data;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.constraints.NotBlank;

@Data
public class PostRequest {
    @Nullable
    private Long postID;
    @NotBlank
    private String tieuDe;
    @JsonProperty("postDetail")
    private PostDetail postDetail;

}
