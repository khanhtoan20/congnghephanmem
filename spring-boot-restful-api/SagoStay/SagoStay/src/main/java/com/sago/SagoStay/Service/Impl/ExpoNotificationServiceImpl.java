package com.sago.SagoStay.Service.Impl;

import com.sago.SagoStay.Model.Notification;
import com.sago.SagoStay.Service.ExpoNotificationService;
import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.message.BasicNameValuePair;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

@Service
public class ExpoNotificationServiceImpl implements ExpoNotificationService {
    @Autowired
    private HttpPost httpPost;
    public void sendMessage(String title,Notification notification) {
        CloseableHttpClient client = HttpClients.createDefault();
        List<NameValuePair> params = new ArrayList<NameValuePair>();
        params.add(new BasicNameValuePair("to", notification.getUser().getExpoNotification().getExpoToken()));
        params.add(new BasicNameValuePair("title", title));
        params.add(new BasicNameValuePair("body", notification.getContent()));
        params.add(new BasicNameValuePair("sound","default"));
        try {
            httpPost.setEntity(new UrlEncodedFormEntity(params,"utf-8"));
            CloseableHttpResponse response = client.execute(httpPost);
            System.out.println(response.getStatusLine().getStatusCode());
        } catch (Exception e) {
            e.printStackTrace();
        }


    }
}
