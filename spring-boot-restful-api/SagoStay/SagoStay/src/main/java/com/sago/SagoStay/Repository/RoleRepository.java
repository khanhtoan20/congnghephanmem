package com.sago.SagoStay.Repository;

import com.sago.SagoStay.Model.Role;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface RoleRepository extends JpaRepository<Role,Long> {
    String findTenQuyenById(Long id);
    boolean existsByTenQuyen(String tenQuyen);
    Role findRoleById(Long id);
}
