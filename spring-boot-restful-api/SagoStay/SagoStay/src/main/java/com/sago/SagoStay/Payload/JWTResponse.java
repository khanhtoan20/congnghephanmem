package com.sago.SagoStay.Payload;

import lombok.Data;
import lombok.RequiredArgsConstructor;

@Data
public class JWTResponse extends  BaseResponseEntity{
    private  String token;
    public JWTResponse(int statusCode,String message, String Token){
        super(statusCode,message);
        this.token=Token;

    }
}
