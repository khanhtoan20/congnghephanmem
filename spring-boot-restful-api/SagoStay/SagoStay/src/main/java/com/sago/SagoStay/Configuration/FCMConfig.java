package com.sago.SagoStay.Configuration;

import com.google.auth.oauth2.GoogleCredentials;
import com.google.firebase.FirebaseApp;
import com.google.firebase.FirebaseOptions;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.ClassPathResource;

import javax.annotation.PostConstruct;

@Configuration
public class FCMConfig {
    @Value("${app.firebase-configuration-file}")
    private String firebasePath;

    @PostConstruct
    public void init(){
        try { FirebaseOptions firebaseOpt = new FirebaseOptions.Builder()
                .setCredentials(GoogleCredentials.fromStream(new ClassPathResource(firebasePath).getInputStream())).build();
            if (FirebaseApp.getApps().isEmpty())
                FirebaseApp.initializeApp(firebaseOpt);
        }catch (Exception ex){

        }


    }
}
