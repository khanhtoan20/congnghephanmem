package com.sago.SagoStay.Payload;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class ExpoToken {
    private String token;
    private Boolean isTurnOn;
}
