package com.sago.SagoStay.Service.Impl;

import com.sago.SagoStay.Model.Staff;
import com.sago.SagoStay.Model.User;
import com.sago.SagoStay.Repository.StaffRepository;
import com.sago.SagoStay.Repository.UserRepository;
import com.sago.SagoStay.Security.StaffDetail;
import com.sago.SagoStay.Security.UserDetail;
import com.sago.SagoStay.Service.StaffDetailService;
import com.sago.SagoStay.Service.UserDetailService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

@Service
public class UserDetailServiceImpl implements StaffDetailService, UserDetailService,UserDetailsService {
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private StaffRepository staffRepository;

    public UserDetail loadUserById(Long ID){
        User user = userRepository.findById(ID).orElseThrow(()->new UsernameNotFoundException(String.format("User %s not found",ID)));
        return UserDetail.create(user);
    }

    public UserDetails loadUserByUsername(String email) {
        User user = userRepository.findByEmail(email);
        Staff staff = staffRepository.findByUsername(email);
        if(user==null && staff !=null) return StaffDetail.create(staff);
        if(user!=null && staff ==null) return UserDetail.create(user);
        return null;
    }

    public StaffDetail loadStaffById(Long ID){
        Staff staff = staffRepository.findById(ID).orElseThrow(()->new UsernameNotFoundException(String.format("Staff %s not found",ID)));
        return StaffDetail.create(staff);
    }

}
