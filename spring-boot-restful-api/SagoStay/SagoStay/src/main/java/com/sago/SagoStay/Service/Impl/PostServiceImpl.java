package com.sago.SagoStay.Service.Impl;

import com.sago.SagoStay.Model.*;
import com.sago.SagoStay.Payload.PostResponse;
import com.sago.SagoStay.Payload.ReportRequest;
import com.sago.SagoStay.Payload.SearchRequest;
import com.sago.SagoStay.Payload.TopViewResponse;
import com.sago.SagoStay.Repository.*;
import com.sago.SagoStay.Service.*;
import com.sago.SagoStay.Specification.PostDetailSpecification;
import com.sago.SagoStay.Specification.SearchCriteria;
import com.sago.SagoStay.Specification.SearchOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.stream.Collectors;

@Service


public class PostServiceImpl implements PostService {
    @Autowired
    private ExpoNotificationService expoNotificationService;
    @Autowired
    private PostRepository postRepository;
    @Autowired
    private PostDetailRepository postDetailRepository;
    @Autowired
    private HistoryService historyService;
    @Autowired
    private NotificationRepository notificationRepository;
    @Autowired
    private StaffService staffService;
    @Autowired
    private ImageRepository imageRepository;
    @Autowired
    private FacilityRepository facilityRepository;
    public PostServiceImpl() {
    }

    public void save(Post post, PostDetail postDetail,List<Image> images) {
        postRepository.save(post);
        postDetailRepository.save(postDetail);
        imageRepository.saveAll(images);
    }
    public void save(Post post){
        postRepository.save(post);
    }
    @Override
    @Transactional
    public void update(Post post,PostDetail postDetail,List<Image> images) {
        postDetail.setId(post.getPostDetail().getId());
        postDetail.setPost(post);
        imageRepository.deleteAllByPostId(post.getId());
        save(post, postDetail,images);
        History history = new History(post, staffService.getSystemStaff());
        historyService.save(history);
        Notification notification = new Notification(post.getUser(), "Bài đăng đang chờ phê duyệtt" + post.getId() + "\\nBài đăng "+post.getTieuDe()+" vừa cập nhật và đang chờ phê duyệt", 1, false);
        expoNotificationService.sendMessage("Đăng bài thành công",notification);
        notificationRepository.save(notification);

    }

    public List<PostResponse> getAllApprovedPosts() {
        List<Post> allPosts = postRepository.findAll();
        return getAllApprovedPostResponse(allPosts);
    }
    public List<PostResponse> getAllPendingPosts() {
        List<Post> allPosts = postRepository.findAll();
        return getAllPendingPostResponse(allPosts);
    }
    public List<PostResponse> getAllRejectedPosts() {
        List<Post> allPosts = postRepository.findAll();
        return getAllRejectedPostResponse(allPosts);
    }
    public List<PostResponse> getAllDeletedPosts() {
        List<Post> allPosts = postRepository.findAll();
        return getAllDeletedPostResponse(allPosts);
    }

    @Override
    public List<PostResponse> getAllApprovedPostsOfUser(Long userID) {
        List<Post> allPosts = postRepository.findAllByUserId(userID);
        return getAllApprovedPostResponse(allPosts);
    }

    @Override
    public List<PostResponse> getAllPendingPostsOfUser(Long userID) {
        List<Post> allPosts = postRepository.findAllByUserId(userID);
        return getAllPendingPostResponse(allPosts);
    }

    @Override
    public List<PostResponse> getAllRejectedPostsOfUser(Long userID) {
        List<Post> allPosts = postRepository.findAllByUserId(userID);
        return getAllRejectedPostResponse(allPosts);
    }

    public List<PostResponse> getAllPosts() {
        List<Post> allPosts = postRepository.findAll();
        return getAllPostResponse(allPosts);
    }

    public List<PostResponse> getAllPostResponse(List<Post> Posts) {
        List<PostResponse> postResponseList = new ArrayList<>(Posts.size());
        for (Post post : Posts) {
            List<Facility> facilities = new ArrayList<>();
            History latestHistory = historyService.getLastestStatus(post.getId());
            PostDetail postDetail = postDetailRepository.findPostByPostId(post.getId());

            Arrays.stream(postDetail.getChuoiTienIch().split("-")).forEach(facility->facilities.add(facilityRepository.findById(facility).get()));
            postResponseList.add(new PostResponse(post.getId(), post.getTieuDe(), post.getCreatedAt(), latestHistory.getTrangThai(), latestHistory.getStaff().getHoTen(), post.getStatus(),postDetail,facilities,post.getImageList().stream().map(image->image.getUrl()).collect(Collectors.toList())));

        }
        return postResponseList;
    }

    public List<PostResponse> getAllApprovedPostResponse(List<Post> Posts){
        List<PostResponse> postResponseList = new ArrayList<>();
        for (Post post : Posts) {
            PostResponse postResponse= getApprovedPostResponse(post);
            if(postResponse != null) {
                postResponseList.add(postResponse);

            }
        }
        return postResponseList;

    }
    public List<PostResponse> getAllDeletedPostResponse(List<Post> Posts) {
        List<PostResponse> postResponseList = new ArrayList<>(Posts.size());
        for (Post post : Posts) {
            if (post.getStatus().equals("Deleted")) {
                PostResponse postResponse = getApprovedPostResponse(post);
                postResponseList.add(postResponse);
            }
        }
        return postResponseList;

    }
    public PostResponse getApprovedPostResponse(Post post){
        History latestHistory = historyService.getLastestStatus(post.getId());
        if(post.getStatus().equals("Available") && latestHistory.getTrangThai().equals("Approved")){
            PostDetail postDetail = postDetailRepository.findPostByPostId(post.getId());
            List<Facility> facilities = new ArrayList<>();
            Arrays.stream(postDetail.getChuoiTienIch().split("-")).forEach(facility->facilities.add(facilityRepository.findById(facility).get()));
            return new PostResponse(post.getId(), post.getTieuDe(), post.getCreatedAt(), latestHistory.getTrangThai(), latestHistory.getStaff().getHoTen(), post.getStatus(),postDetail,facilities,post.getImageList().stream().map(image->image.getUrl()).collect(Collectors.toList()));
        }
        return null;
    }

    public List<PostResponse> getAllPendingPostResponse(List<Post> Posts){
        List<PostResponse> postResponseList = new ArrayList<>(Posts.size());
        for (Post post : Posts) {
            History latestHistory = historyService.getLastestStatus(post.getId());
            if(post.getStatus().equals("Available") && latestHistory.getTrangThai().equals("Pending")) {
                PostDetail postDetail = postDetailRepository.findPostByPostId(post.getId());
                List<Facility> facilities = new ArrayList<>();
                Arrays.stream(postDetail.getChuoiTienIch().split("-")).forEach(facility->facilities.add(facilityRepository.findById(facility).get()));
                postResponseList.add(new PostResponse(post.getId(), post.getTieuDe(), post.getCreatedAt(), latestHistory.getTrangThai(), latestHistory.getStaff().getHoTen(), post.getStatus(),postDetail,facilities,post.getImageList().stream().map(image->image.getUrl()).collect(Collectors.toList())));
            }
        }
        return postResponseList;

    }
    public List<PostResponse> getAllRejectedPostResponse(List<Post> Posts){
        List<PostResponse> postResponseList = new ArrayList<>(Posts.size());
        for (Post post : Posts) {
            History latestHistory = historyService.getLastestStatus(post.getId());
            if(post.getStatus().equals("Available") && latestHistory.getTrangThai().equals("Rejected")) {
                PostDetail postDetail = postDetailRepository.findPostByPostId(post.getId());
                List<Facility> facilities = new ArrayList<>();

                Arrays.stream(postDetail.getChuoiTienIch().split("-")).forEach(facility->facilities.add(facilityRepository.findById(facility).get()));

                postResponseList.add(new PostResponse(post.getId(), post.getTieuDe(), post.getCreatedAt(), latestHistory.getTrangThai(), latestHistory.getStaff().getHoTen(), post.getStatus(),postDetail,facilities,post.getImageList().stream().map(image->image.getUrl()).collect(Collectors.toList())));
            }
        }
        return postResponseList;

    }

    @Override
    public boolean existsById(Long postID) {
        return postRepository.existsById(postID);
    }

    @Override
    public Post getPostById(Long postID) {
        return postRepository.findById(postID).get();
    }

    @Override
    public PostResponse getPostResponse(Long postID) {
        if(existsById(postID)){
            Post currentPost = postRepository.findById(postID).get();
            if(currentPost.getStatus().equals("Deleted")) return null;
            History latestHistory = historyService.getLastestStatus(currentPost.getId());
            PostDetail postDetail = postDetailRepository.findPostByPostId(currentPost.getId());
            List<Facility> facilities = new ArrayList<>();

            Arrays.stream(postDetail.getChuoiTienIch().split("-")).forEach(facility->facilities.add(facilityRepository.findById(facility).get()));

            if(latestHistory.getTrangThai().equals("Approved")) {
                currentPost.setLuotXem(currentPost.getLuotXem() + 1);
                save(currentPost);
            }
            return new PostResponse(currentPost.getId(), currentPost.getTieuDe(), currentPost.getCreatedAt(), latestHistory.getTrangThai(), latestHistory.getStaff().getHoTen(), currentPost.getStatus(),postDetail,facilities,currentPost.getImageList().stream().map(image->image.getUrl()).collect(Collectors.toList()));

        }
        return null;
    }
    @Override
    public List<Integer> statistic() {
        List<Post> totalPost = postRepository.findAll();
        List<Integer> result = new ArrayList<>(4);
        result.add(getAllApprovedPostResponse(totalPost).size());
        result.add(getAllPendingPostResponse(totalPost).size());
        result.add(getAllRejectedPostResponse(totalPost).size());
        result.add(getAllDeletedPostResponse(totalPost).size());
        return result;
    }
    public List<PostResponse> getAllPostsByUserId(Long userID) {
        List<Post> allPosts = postRepository.findAllByUserId(userID);
        return getAllApprovedPostResponse(allPosts);
    }
    @Override
    public void create(Post post, PostDetail currentpostDetail,List<Image> images) {
        PostDetail postDetail = new PostDetail(currentpostDetail);
        postDetail.setPost(post);
        post.setStatus("Available");
        save(post, postDetail,images);
        History history = new History(post, staffService.getSystemStaff());
        historyService.save(history);
        Notification notification = new Notification(post.getUser(), "Bài đăng đang chờ phê duyệt" + "\\nBài đăng "+post.getTieuDe()+" đang chờ phê duyệt", 1, false);
        expoNotificationService.sendMessage("Đăng bài thành công",notification);
        notificationRepository.save(notification);
    }

    @Override
    public void delete(Post post) {
        post.setStatus("Deleted");
        save(post);
        Notification notification = new Notification(post.getUser(), post.getTieuDe()  + "\\nXóa bài thành công", 1, false);
        expoNotificationService.sendMessage("Xóa bài thành công",notification);
        notificationRepository.save(notification);
    }
    public List<PostDetail> filterPostDetailByCriteria(SearchRequest criteria)
    {
        Iterator<String> facilities = Arrays.stream(criteria.getFacility().split("-")).iterator();
        Specification facilitySpec = new PostDetailSpecification(
                new SearchCriteria("chuoiTienIch", SearchOperation.LIKE, facilities.next()));
        while(facilities.hasNext()) {
            facilitySpec = Specification.where(facilitySpec).or(
                    new PostDetailSpecification(new SearchCriteria("chuoiTienIch", SearchOperation.LIKE, facilities.next())
                    )
            );
        }
        Iterator<String> districts = criteria.getDistricts().iterator();
        Specification districtSpec = new PostDetailSpecification(
                new SearchCriteria("diaChi", SearchOperation.CONTAINS, districts.next()));
        while(districts.hasNext()) {
            districtSpec = Specification.where(districtSpec).or(
                    new PostDetailSpecification(new SearchCriteria("diaChi", SearchOperation.CONTAINS, districts.next())
                    )
                    );
        }
        return postDetailRepository.findAll(Specification.where(
                districtSpec.and(facilitySpec).and(districtSpec)));
    }
    @Override
    public List<Post> getAdvancedSearch(SearchRequest searchRequest){
        List<PostDetail> filteredPost = filterPostDetailByCriteria(searchRequest);
        System.out.println("Filtered[district|facility]: "+filteredPost.size());
        Long minCost = searchRequest.getMinPrice();
        Long maxCost = searchRequest.getMaxPrice();
        Long minArea = searchRequest.getMinArea();
        Long maxArea = searchRequest.getMaxArea();
        Long cost,area;
        for (int i = 0 ;i<filteredPost.size();i++){
            cost = Long.parseLong(filteredPost.get(i).getGia().replace(".",""));
            area = Long.parseLong(filteredPost.get(i).getDienTich());
            if( cost > maxCost || cost < minCost){
                System.out.println("Invalid cost:"+cost+" max:"+maxCost+" min:"+minCost);
                filteredPost.remove(i);
                i--;
                System.out.println(filteredPost.size());
            }
            else {
            if( area > maxArea || area < minArea){
                System.out.println("Invalid area:"+area+" max:"+maxArea+" min:"+minArea);
                filteredPost.remove(i);
                i--;
                System.out.println(filteredPost.size());
            }}
        }
        System.out.println("EndFiltered[cost|area]: "+filteredPost.size());
        List<Post> postList = new ArrayList<>(filteredPost.size());
        for(PostDetail postDetail : filteredPost)
        {
            Post post = postRepository.getOne(postDetail.getPost().getId());
            postList.add(post);
        }
        return postList;
    }
    public List<Post> getTopXViewPost() {
        List<Post> viewSortedPosts = postRepository.findAll(Sort.by(Sort.Direction.DESC, "luotXem"));
        return viewSortedPosts;
    }
    @Override
    public List<TopViewResponse> getTopViewResponse(int x)
    {
        List<Post> viewSortedPosts = getTopXViewPost();
        int maxTopX = viewSortedPosts.size();
        if ( x < 0)
            {
                x = 1;
            }
        else
            if ( x > maxTopX )
            {
                x = maxTopX;
            }
        List<TopViewResponse> responses = new ArrayList<>(x);
        for(int i = 0 ; i < x ; i++)
        {
            responses.add(new TopViewResponse(
                    viewSortedPosts.get(i).getId(),
                    viewSortedPosts.get(i).getTieuDe(),
                    viewSortedPosts.get(i).getLuotXem()
            ));
        }
        return  responses;
    }
}
