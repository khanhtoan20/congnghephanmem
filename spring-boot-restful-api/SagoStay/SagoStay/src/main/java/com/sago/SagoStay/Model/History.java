package com.sago.SagoStay.Model;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;

@Entity
@Data
@Table(name="staff_post_history")
@NoArgsConstructor

@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id")
public class History extends DateEntity{
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotBlank
    @ManyToOne(targetEntity = Post.class)
    @JoinColumn(name="post_id",nullable = false)
    private  Post post;

    @NotBlank
    @ManyToOne(targetEntity = Staff.class)
    @JoinColumn(name="staff_id",nullable = false)
    private  Staff staff;

    @NotBlank
    @Column(name="trangthai",columnDefinition = "varchar(255) default 'Pending'")
    private String trangThai="Pending";

    @Column(name="lido")
    private String liDo;
    public History(Post post, Staff staff){
        this.post = post;
        this.staff = staff;

    }
    public History(Post post, Staff staff, String trangThai, String liDo){
        this.post = post;
        this.staff = staff;
        this.trangThai = trangThai;
        this.liDo = liDo;
    }
}
