package com.sago.SagoStay.Configuration.DataLoader;

import com.sago.SagoStay.Model.Facility;
import com.sago.SagoStay.Model.Role;
import com.sago.SagoStay.Repository.FacilityRepository;
import com.sago.SagoStay.Service.RoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.stereotype.Component;

import javax.transaction.Transactional;

@Component
public class FacilityLoader implements ApplicationListener<ContextRefreshedEvent> {

    @Autowired
    private FacilityRepository facilityRepository;
    private boolean dataLoaded = false;
    @Override
    public void onApplicationEvent(ContextRefreshedEvent contextRefreshedEvent) {
        if(dataLoaded) return;

        createFacIfNotFound("ml","Máy lạnh","url");
        createFacIfNotFound("nvsr","Nhà vệ sinh riêng","url");
        createFacIfNotFound("bdx","Bãi để xe","url");
        createFacIfNotFound("wf","Wifi","url");
        dataLoaded = true;
    }
    @Transactional
    void createFacIfNotFound(String MaTienIch,String TenTienIch,String Url){
        if(facilityRepository.existsByMaTienIch(MaTienIch)) return;
        facilityRepository.save(new Facility(MaTienIch,TenTienIch,Url));
    }
}