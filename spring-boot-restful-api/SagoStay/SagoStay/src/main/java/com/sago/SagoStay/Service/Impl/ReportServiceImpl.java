package com.sago.SagoStay.Service.Impl;

import com.sago.SagoStay.Model.*;
import com.sago.SagoStay.Payload.PostResponse;
import com.sago.SagoStay.Payload.ReportResponse;
import com.sago.SagoStay.Repository.NotificationRepository;
import com.sago.SagoStay.Repository.ReportRepository;
import com.sago.SagoStay.Service.ReportService;
import com.sago.SagoStay.Service.StaffService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class ReportServiceImpl implements ReportService {
    @Autowired
    private ReportRepository reportRepository;
    @Autowired
    private NotificationRepository notificationRepository;
    @Autowired
    private StaffService staffService;
    @Override
    public void save(Report report)
    {
        reportRepository.save(report);
    }
    @Override
    public Report findById(Long id)
    {
        return reportRepository.findById(id).get();
    }
    @Override
    public List<Report> getAllReport()
    {
        return reportRepository.findAll();
    }
    @Override
    public List<ReportResponse> getAllPendingReports() {
        List<Report> allReports = reportRepository.findAll();
        return getAllPendingReportResponse(allReports);
    }
    public ReportResponse getPendingReportResponse(Report report){
        if(report.getStatus().equals("Pending"))
        {
            return new ReportResponse(report.getId(),report.getReason(),report.getPost().getId(),report.getUser().getId(),report.getStatus(),report.getCreatedAt());
        }
        return null;
    }
    @Override
    public List<ReportResponse> getAllPendingReportResponse(List<Report> reports){
        List<ReportResponse> reportResponseList = new ArrayList<>(reports.size());
        for (Report report : reports) {
            ReportResponse reportResponse = getPendingReportResponse(report);
            if(reportResponse != null)
                reportResponseList.add(reportResponse);
        }
        return reportResponseList;
    }
}
