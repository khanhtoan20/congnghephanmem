package com.sago.SagoStay.Payload;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import lombok.AllArgsConstructor;
import lombok.Data;

@JsonPropertyOrder({
        "status_code",
        "message"
})
@Data
@AllArgsConstructor
public class BaseResponseEntity {
    @JsonProperty
    private int statusCode;
    @JsonProperty
    private String message;
}
