package com.sago.SagoStay.Service;

public interface EmailService {
    void sendingVerifiedEmail(String To,String Token);
    void recoveryPasswordEmail(String To,String context);
}
