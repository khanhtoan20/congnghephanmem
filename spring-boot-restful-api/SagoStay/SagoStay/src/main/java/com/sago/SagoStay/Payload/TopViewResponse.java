package com.sago.SagoStay.Payload;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.sago.SagoStay.Model.Facility;
import com.sago.SagoStay.Model.PostDetail;
import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.Date;
import java.util.List;

@Data
@AllArgsConstructor
public class TopViewResponse {
    @JsonProperty
    private Long id;
    @JsonProperty
    private String tieuDe;
    @JsonProperty
    private Long luotXem;
}
