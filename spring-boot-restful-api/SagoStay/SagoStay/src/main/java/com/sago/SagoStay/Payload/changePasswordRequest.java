package com.sago.SagoStay.Payload;

import lombok.Data;

import javax.validation.constraints.NotBlank;
@Data
public class changePasswordRequest {
    @NotBlank
    private String oldPassword;
    @NotBlank
    private String newPassword;
}
