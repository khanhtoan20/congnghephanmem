package com.sago.SagoStay.Payload;

import lombok.Data;
import org.codehaus.jackson.annotate.JsonPropertyOrder;

import javax.validation.constraints.NotBlank;

@JsonPropertyOrder({
        "hoTen",
        "username",
        "password",
        "namSinh",
        "soDienThoai",
        "luong",
        "roleType"
})
@Data
public class StaffUpdateRequest {
    @NotBlank
    private String hoTen;

    @NotBlank
    private String username;

    @NotBlank
    private String soDienThoai;

    @NotBlank
    private Long luong;

    @NotBlank
    private Long roleType;

    @NotBlank
    private String namSinh;
}
