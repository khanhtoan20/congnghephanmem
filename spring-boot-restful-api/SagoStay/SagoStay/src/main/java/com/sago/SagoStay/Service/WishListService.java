package com.sago.SagoStay.Service;

import com.sago.SagoStay.Model.Post;
import com.sago.SagoStay.Model.User;
import com.sago.SagoStay.Model.WishList;

import java.util.List;
import java.util.Optional;

public interface WishListService {
    void save(WishList wishList);
    void delete(WishList wishList);
    WishList findWishList(Long userId,Long postId);
    List<Post> getMyWishList(Long id);
}
