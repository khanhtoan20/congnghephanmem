package com.sago.SagoStay.Service;

import com.sago.SagoStay.Model.Role;

public interface RoleService {
    Role findRoleById(Long id);
    String findTenQuyenById(Long id);
    void save(Role role);
    boolean existsByTenQuyen(String tenQuyen);
}