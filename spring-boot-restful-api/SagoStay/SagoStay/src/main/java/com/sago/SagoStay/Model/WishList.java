package com.sago.SagoStay.Model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Entity
@NoArgsConstructor
@AllArgsConstructor
@Data
@Table(name="wishlist")
@IdClass(WishListID.class)
public class WishList{
    @Id
    @NotBlank
    private Long userId;

    @Id
    @NotBlank
    private Long postId;

}
