package com.sago.SagoStay.Repository;

import com.sago.SagoStay.Model.Post;
import com.sago.SagoStay.Model.PostDetail;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

@Repository
public interface PostDetailRepository extends JpaRepository<PostDetail,Long>,
         JpaSpecificationExecutor<PostDetail> {
    PostDetail findPostByPostId(Long id);
}
