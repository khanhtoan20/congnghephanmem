package com.sago.SagoStay.Model;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Data
@Entity
@Table(name="role")
@NoArgsConstructor
@RequiredArgsConstructor
public class Role extends DateEntity{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="id")
    private Long id;

    @NotBlank
    @Column(name="tenquyen")
    @NonNull
    private String tenQuyen;
    @OneToMany(fetch = FetchType.LAZY,mappedBy="role")
    private List<Staff> listStaff = new ArrayList<>();
}
