package com.sago.SagoStay.Controller;

import com.sago.SagoStay.Model.Notification;
import com.sago.SagoStay.Model.Post;
import com.sago.SagoStay.Model.Report;
import com.sago.SagoStay.Model.User;
import com.sago.SagoStay.Payload.BaseResponseEntity;
import com.sago.SagoStay.Payload.ReportRequest;
import com.sago.SagoStay.Payload.ReportResponse;
import com.sago.SagoStay.Payload.UserUpdateRequest;
import com.sago.SagoStay.Repository.NotificationRepository;
import com.sago.SagoStay.Security.StaffDetail;
import com.sago.SagoStay.Security.UserDetail;
import com.sago.SagoStay.Service.ExpoNotificationService;
import com.sago.SagoStay.Service.PostService;
import com.sago.SagoStay.Service.ReportService;
import com.sago.SagoStay.Service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/api/reports")
public class ReportController {
    @Autowired
    private ReportService reportService;
    @Autowired
    private PostService postService;
    @Autowired
    private UserService userService;
    @PostMapping()
    @PreAuthorize("hasAuthority('MEMBER')")
    public ResponseEntity<BaseResponseEntity> report(@Valid @RequestBody ReportRequest reportRequest, @AuthenticationPrincipal UserDetail userDetail) {
        Post post = postService.getPostById(reportRequest.getPostId());
        User currentUser = userService.findByEmail(userDetail.getEmail());
        Report report = new Report();
        report.setPost(post);
        report.setUser(currentUser);
        report.setReason(reportRequest.getReason());
        report.setStatus("Pending");
        reportService.save(report);
        return new ResponseEntity<>(new BaseResponseEntity(1, "Đã báo cáo bài viết"), HttpStatus.OK);
    }
    @GetMapping()
    @PreAuthorize("hasAuthority('1') or hasAuthority('4')")
    public ResponseEntity<BaseResponseEntity> getAllReports()
    {
        try{
            return new ResponseEntity(reportService.getAllReport(),HttpStatus.OK);
        }catch(AccessDeniedException ex){
            return new ResponseEntity<>(new BaseResponseEntity(403, "Bạn không có quyền truy cập"), HttpStatus.OK);
        }
    }
    @GetMapping("/pending")
    @PreAuthorize("hasAuthority('1') or hasAuthority('4')")
    public ResponseEntity<List<ReportResponse>> getAllPendingReports()
    {
        return new ResponseEntity(reportService.getAllPendingReports(),HttpStatus.OK);
    }
}
