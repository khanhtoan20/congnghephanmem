package com.sago.SagoStay.Service.Impl;

import com.sago.SagoStay.Model.*;
import com.sago.SagoStay.Payload.NotifyAllRequest;
import com.sago.SagoStay.Payload.StaffResponse;
import com.sago.SagoStay.Repository.NotificationRepository;
import com.sago.SagoStay.Repository.ReportRepository;
import com.sago.SagoStay.Repository.StaffRepository;
import com.sago.SagoStay.Repository.UserRepository;
import com.sago.SagoStay.Security.JWTokenProvider;
import com.sago.SagoStay.Service.ExpoNotificationService;
import com.sago.SagoStay.Service.HistoryService;
import com.sago.SagoStay.Service.StaffService;
import com.sago.SagoStay.Service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.List;

@Service
public class StaffServiceImpl implements StaffService {
    @Autowired
    private StaffRepository staffRepository;
    @Autowired
    private ReportRepository reportRepository;
    @Autowired
    private JWTokenProvider jwTokenProvider;
    @Autowired
    private ExpoNotificationService expoNotificationService;
    @Autowired
    private NotificationRepository notificationRepository;
    @Autowired
    private HistoryService historyService;
    @Autowired
    private UserService userService;
    @Override
    public void save(Staff Staff) {
        staffRepository.save(Staff);
    }
    @Override
    public Staff getStaff(String token) {
        return null;
    }

    @Override
    public boolean existsByUsername(String username) {
        return staffRepository.existsByUsername(username);
    }

    @Override
    public Staff findByUsername(String username) {
        return staffRepository.findByUsername(username);
    }

    @Override
    public void updateSoDienThoai(Long id, String soDienThoai) {

    }

    @Override
    public Staff findById(Long id) {
        return staffRepository.findStaffById(id);
    }

    @Override
    public Staff JWTokenToStaff(HttpServletRequest httpServletRequest) {
        String token = httpServletRequest.getHeader("Authorization").substring(7);
        return staffRepository.findById(jwTokenProvider.getIDByJWT(token)).orElseThrow(()-> new UsernameNotFoundException("not found"));
    }
    public  Staff getSystemStaff(){
        return staffRepository.findStaffById((0L));
    }

    @Override
    public List<Staff> getAllStaff()
    {
        return staffRepository.findAll();
    }
    @Override
    public List<StaffResponse> getAllStaffResponse() {
        List<Staff> staffList = staffRepository.findAll();
        List<StaffResponse> staffResponses = new ArrayList<>(staffList.size());
        for(Staff staff : staffList)
        {
            StaffResponse response = new StaffResponse(
                    staff.getId(),
                    staff.getHoTen(),
                    staff.getUsername(),
                    staff.getSoDienThoai(),
                    staff.getNamSinh(),
                    staff.getLuong(),
                    staff.getRole().getId(),
                    staff.getCreatedAt(),
                    staff.getUpdatedAt()
            );
            staffResponses.add(response);
        }
        return staffResponses;
    }
    @Override
    public void approvingPost(History history) {
        historyService.save(history);
        Notification notification = new Notification(history.getPost().getUser(), "Bài viết :" + history.getPost().getTieuDe() + "\nBài đăng của bạn đã được phê duyệt \n", 1, false);
        notificationRepository.save(notification);
        expoNotificationService.sendMessage(history.getTrangThai(),notification);
    }
    @Override
    public void handleReport(Report report) {
        reportRepository.save(report);
        Notification notification = new Notification(report.getUser(), "Bài viết: " + report.getPost().getTieuDe() + "\nBáo cáo của bạn đã được xử lí\n Lí do: "+report.getReason(), 1, false);
        notificationRepository.save(notification);
        expoNotificationService.sendMessage(report.getReason(),notification);
    }
    @Override
    public void banUser(Long userID) {
            User currentUser = userService.findById(userID);
            currentUser.setLocked(true);
            userService.save(currentUser);
    }
    @Override
    public void unbanUser(Long userID){
        User currentUser = userService.findById(userID);
        currentUser.setLocked(false);
        userService.save(currentUser);
    }
    @Override
    public void notifyAll(NotifyAllRequest notifyAllRequest){
        List<User> currentUserLists = userService.getAllUser();
        List<Notification> notifications = new ArrayList<>();
        for ( User user : currentUserLists){
            Notification notification = new Notification(
                    user,
                    notifyAllRequest.getTieuDe() + "\n" +notifyAllRequest.getNoiDung(),
                    1,
                    false);
            notificationRepository.save(notification);
            notifications.add(notification);
        }
        for ( Notification noti : notifications){
            expoNotificationService.sendMessage("SAGOSTAY",noti);
        }
    }
}
