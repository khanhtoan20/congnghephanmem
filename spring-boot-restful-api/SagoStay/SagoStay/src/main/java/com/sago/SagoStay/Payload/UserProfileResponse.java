package com.sago.SagoStay.Payload;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.List;

@JsonPropertyOrder({
        "hoTen",
        "soDienThoai",
        "email",
        "danhSachBaiDang"
})
@Data
@AllArgsConstructor
public class UserProfileResponse{
    @JsonProperty
    private String hoTen;
    @JsonProperty
    private String soDienThoai;
    @JsonProperty
    private String email;
    @JsonProperty
    private List<PostResponse> danhSachBaiDang;
}

