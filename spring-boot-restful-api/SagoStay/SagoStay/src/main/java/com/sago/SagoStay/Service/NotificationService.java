package com.sago.SagoStay.Service;

import com.sago.SagoStay.Model.Notification;

import java.util.List;

public interface NotificationService {
    void save(Notification notification);
    List<Notification> getAllNotificationsByUserId(Long userID);
    String countUnreadNotification(Long userID);
}
