package com.sago.SagoStay.Service.Impl;

import com.sago.SagoStay.Model.History;
import com.sago.SagoStay.Repository.HistoryRepository;
import com.sago.SagoStay.Service.HistoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class HistoryServiceImpl implements HistoryService {
    @Autowired
    private HistoryRepository staffPostRepository;
    @Override
    public void save(History history) {
        staffPostRepository.save(history);
    }

    @Override
    public History getLastestStatus(Long id) {
        return staffPostRepository.findByPostIdOrderByIdDesc(id).get(0);
    }

}
