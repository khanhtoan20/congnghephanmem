package com.sago.SagoStay.Model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.NaturalId;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

@Data
@Entity
@NoArgsConstructor

@Table(name="staff",uniqueConstraints ={@UniqueConstraint(columnNames = {"username"})})
public class Staff extends DateEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @NotBlank
    @Column(name = "hoten")

    private String hoTen;

    @JsonIgnore
    @NotBlank
    @Size(max = 100)
    @Column(name = "password")
    private String password;

    @NotBlank
    @NaturalId
    @Size(max = 40)
    @Column(name = "username")
    private String username;

    @Column(name = "sodienthoai")
    private String soDienThoai;
    @Size(max = 4)
    @Column(name = "namsinh")
    private String namSinh;

    @Column(name = "luong")
    private Long luong;

    @JsonIgnore
    @ManyToOne(targetEntity = Role.class)
    @JoinColumn(name="role_id",nullable = false)
    private Role role;

    public Staff(String hoTen,String username,String password,String soDienThoai,String namSinh,Long luong,Role role){
        this.hoTen = hoTen;
        this.soDienThoai = soDienThoai;
        this.username =username;
        this.password = password;
        this.luong = luong;
        this.role = role;
        this.namSinh = namSinh;
    }

    public Role getRole(){
        return this.role;
    }
}
