package com.sago.SagoStay.Service;

import com.sago.SagoStay.Model.Image;
import com.sago.SagoStay.Model.Post;
import com.sago.SagoStay.Model.PostDetail;
import com.sago.SagoStay.Payload.PostResponse;
import com.sago.SagoStay.Payload.SearchRequest;
import com.sago.SagoStay.Payload.TopViewResponse;

import java.util.List;
import java.util.Optional;

public interface PostService {
    void save(Post post,PostDetail postDetail,List<Image> images);
    void update(Post post,PostDetail postDetail,List<Image> images);
    void create(Post post, PostDetail postDetail,List<Image> images);
    void delete(Post post);
    List<PostResponse> getAllPostResponse(List<Post> postList);
    List<PostResponse> getAllPosts();
    List<PostResponse> getAllApprovedPosts();
    List<PostResponse> getAllPendingPosts();
    List<PostResponse> getAllRejectedPosts();
    List<PostResponse> getAllDeletedPosts();
    List<PostResponse> getAllApprovedPostsOfUser(Long userID);
    List<PostResponse> getAllPendingPostsOfUser(Long userID);
    List<PostResponse> getAllRejectedPostsOfUser(Long userID);
    List<PostResponse> getAllApprovedPostResponse(List<Post> postList);
    List<PostResponse> getAllPendingPostResponse(List<Post> postList);
    List<PostResponse> getAllRejectedPostResponse(List<Post> postsList);
    boolean existsById(Long postID);
    Post getPostById(Long postID);
    PostResponse getPostResponse(Long postID);
    List<Integer> statistic();
    List<Post> getAdvancedSearch(SearchRequest searchRequest);
    List<TopViewResponse> getTopViewResponse(int x);
}
