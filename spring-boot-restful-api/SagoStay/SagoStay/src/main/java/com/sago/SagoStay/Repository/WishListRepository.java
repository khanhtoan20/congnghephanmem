package com.sago.SagoStay.Repository;

import com.sago.SagoStay.Model.Post;
import com.sago.SagoStay.Model.User;
import com.sago.SagoStay.Model.WishList;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;
@Repository
public interface WishListRepository extends JpaRepository<WishList,Long> {
    List<WishList> findByUserId(Long id);
}
