package com.sago.SagoStay.Payload;

import com.sun.istack.Nullable;
import lombok.Data;
import org.codehaus.jackson.annotate.JsonPropertyOrder;
import javax.validation.constraints.NotBlank;
@Data
@JsonPropertyOrder({
        "postId",
        "reason",
})
public class ReportRequest {
    @NotBlank
    private Long postId;
    @NotBlank
    private String reason;
}
