package com.sago.SagoStay.Payload;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.List;
@JsonPropertyOrder({
        "wishList"
})
@Data
@AllArgsConstructor
public class WishListResponse {
    @JsonProperty
    private List<PostResponse> myWishList;
}
