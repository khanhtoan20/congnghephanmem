package com.sago.SagoStay.Payload;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.Date;

@Data
@AllArgsConstructor
public class StaffResponse {
    @JsonProperty
    private Long id;
    @JsonProperty
    private String hoTen;
    @JsonProperty
    private String username;
    @JsonProperty
    private String soDienThoai;
    @JsonProperty
    private String namSinh;
    @JsonProperty
    private Long Luong;
    @JsonProperty
    private Long roleType;
    @JsonProperty
    private Date createAt;
    @JsonProperty
    private Date updateAt;
}
