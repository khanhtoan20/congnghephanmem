package com.sago.SagoStay.Payload;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.sago.SagoStay.Model.Facility;
import com.sago.SagoStay.Model.PostDetail;
import lombok.AllArgsConstructor;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import java.util.Date;
import java.util.List;

@Data
@AllArgsConstructor
public class NotifyAllRequest {
    @JsonProperty
    private String tieuDe;
    @JsonProperty
    private String noiDung;
}
