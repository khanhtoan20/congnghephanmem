package com.sago.SagoStay.Payload;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class ApprovePostRequest {
    public Long postID;
    public String trangThai;
    public String liDo;
}
