package com.sago.SagoStay.Model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Data
@Entity
@AllArgsConstructor
@NoArgsConstructor
public class Facility {
    @Id
    @Column(name="matienich")
    private String maTienIch;

    @Column(name="tenTienIch")
    private String tenTienIch;

    @Column(name="url")
    private String imgUrl;
}
