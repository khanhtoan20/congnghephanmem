package com.sago.SagoStay.Service;

import com.sago.SagoStay.Security.UserDetail;

public interface UserDetailService{
    UserDetail loadUserById(Long ID);
}
