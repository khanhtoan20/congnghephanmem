import React from "react";
import "../assets/css/styles.css";
import "react-bootstrap";
import Header from "../components/Header/Header.js";
import {Card,Nav} from "react-bootstrap";
import BanAccount from "../components/TableAccount/BanAccount";
import ListAccount from "../components/TableAccount/ListAccount";
import UnBanAccount from "../components/TableAccount/UnBanAccount";
import { BrowserRouter as Router,Switch, Route,NavLink} from "react-router-dom";
function ManageAccount() {
    return (
        <Router>
        <div className="container-fluid ">
                <div className="row rowContent">
                    <div className="mainContent col-lg-12">
                        <Header title="Quản lý tài khoản người dùng"/>
                        <Nav className="navContentTable" >
                                <Nav.Item className="navItem">
                                    <NavLink to="/listaccount"
                                            className="navLink"
                                            activeStyle={{
                                                fontWeight: "bold",
                                                padding: "10px",
                                                backgroundColor: "#007bff",
                                                color: "#fff",
                                                borderRadius: "5px"
                                            }}
                                    >
                                        Danh sách tài khoản
                                    </NavLink>
                                </Nav.Item>
                                <Nav.Item className="navItem">
                                    <NavLink to="/blockaccount" 
                                            exact
                                            className="navLink"
                                            activeStyle={{
                                            fontWeight: "bold",
                                            padding: "10px",
                                            backgroundColor: "#007bff",
                                            color: "#fff",
                                            borderRadius : "5px"
                                        }}
                                        >
                                            Khóa tài khoản
                                    </NavLink>
                                </Nav.Item>
                                <Nav.Item className="navItem">
                                    <NavLink to="/unblockaccount" 
                                            exact
                                            className="navLink"
                                            activeStyle={{
                                            fontWeight: "bold",
                                            padding: "10px",
                                            backgroundColor: "#007bff",
                                            color: "#fff",
                                            borderRadius : "5px"
                                        }}
                                        >
                                            Mở khóa tài khoản
                                    </NavLink>
                                </Nav.Item>
                        </Nav>
                    </div>
                </div>
                <Switch>
                    <Route path="/listaccount">
                        <Card className="cardContentTable">
                            <ListAccount/>
                        </Card>
                    </Route>
                    <Route path="/blockaccount">
                        <Card className="cardContentTable">
                            <BanAccount/>
                        </Card>
                    </Route>
                    <Route path="/unblockaccount">
                        <Card className="cardContentTable">
                            <UnBanAccount/>
                        </Card>
                    </Route>
                </Switch>
        </div> 
        </Router>
    );
}
export default ManageAccount;