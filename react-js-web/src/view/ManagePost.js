import React from "react";
import "../assets/css/styles.css";
import Header from "../components/Header/Header.js";
import {Card,Nav} from "react-bootstrap";
import ConfirmPost from "../components/TablePost/ConfirmPost";
import ListPost from "../components/TablePost/ListPost";
import { BrowserRouter as Router,Switch, Route,NavLink} from "react-router-dom";
function ManagePost(){
    return(
        <Router>
            <div className="container-fluid">
                <div className="row rowContent">
                    <div className="mainContent col-lg-12">
                    <Header title="Phê duyệt bài viết"/>
                        <Nav className="navContentTable" >
                                <Nav.Item className="navItem">
                                    <NavLink to="/post"
                                            className="navLink"
                                            activeStyle={{
                                                fontWeight: "bold",
                                                padding: "10px",
                                                backgroundColor: "#007bff",
                                                color: "#fff",
                                                borderRadius: "5px"
                                            }}
                                    >
                                        Danh sách bài viết
                                    </NavLink>
                                </Nav.Item>
                                <Nav.Item className="navItem">
                                    <NavLink to="/comfirmpost" 
                                            exact
                                            className="navLink"
                                            activeStyle={{
                                            fontWeight: "bold",
                                            padding: "10px",
                                            backgroundColor: "#007bff",
                                            color: "#fff",
                                            borderRadius : "5px"
                                        }}
                                        >
                                            Phê duyệt bài viết
                                    </NavLink>
                                </Nav.Item>
                        </Nav>
                    </div>
                </div>
                <Switch>
                    <Route path="/post">
                        <Card className="cardContentTable">
                            <ListPost/>
                        </Card>
                    </Route>
                    <Route path="/comfirmpost">
                        <Card className="cardContentTable">
                            <ConfirmPost/>
                        </Card>
                    </Route>
                </Switch>
            </div>
        </Router>            
        );

}
export default ManagePost;