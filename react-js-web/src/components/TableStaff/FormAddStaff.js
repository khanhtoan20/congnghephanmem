import React from "react";
import axios from "axios";
import {Form,Col,Button} from "react-bootstrap";
import "../../assets/css/styles.css";
const role = [
    {"id" : 1,"tenquyen":"Hệ thống"},
    {"id" : 2,"tenquyen":"Quản lý nhân viên"},
    {"id" : 3,"tenquyen":"Quản lý bài đăng"},
    {"id" : 4,"tenquyen":"Chăm sóc khách hàng"},
    {"id" : 5,"tenquyen":"Quản lý tài khoản"},
    ]

class FormAddStaff extends React.Component{
    constructor(props){
        super(props);
        this.state = {
            txtName : null,
            txtBirthDay : null,
            txtPhoneNumber: null,
            txtUsername: null,
            txtWage: null,
            inputRole: null,
            data: [],
            validated : false
        }
    }
    componentDidMount(){
        const token = localStorage.token;
        if(token){
        axios.get("http://localhost:8187/api/staffs",{
                headers: {
                    'Authorization': `Bearer ${token}`
                }
            }).then(response =>{
                this.setState({
                    data : response.data
                })
                }
            )
        }
    }
    isInputChange = (event) =>{
        const target = event.target;
        const name = target.name;
        const value = target.value;
        this.setState({
            [name] : value
        });
    }
    submitFormAdd(event){
        const token = localStorage.token;
        const form = event.currentTarget;
        if (form.checkValidity() === false) {
            event.preventDefault();
        }
        else{
            event.preventDefault();
            var content ={
                hoTen : this.state.txtName,
                username: this.state.txtUsername,
                namSinh : this.state.txtBirthDay,
                soDienThoai : this.state.txtPhoneNumber,
                luong: this.state.txtWage,
                roleType: this.state.inputRole
            };
            if(token){
            fetch("http://localhost:8187/api/staffs",{
                    method : "POST",
                    headers :{
                        "Content-Type" : "application/json",
                        'Authorization': `Bearer ${token}`,
                    },
                    body : JSON.stringify(content)
                }).then(res => res.json()).then(response =>{
                    alert(response.message)
                })
            }
        }
        this.setState({
            validated : true
        })
    }
    render(){
        return(
            
            <Form className="container formAdd" noValidate validated={this.state.validated} onSubmit={(event)=>this.submitFormAdd(event)}>
                <Form.Row>
                    <Form.Group as={Col} controlId="formGridName">
                        <Form.Label>Họ và Tên</Form.Label>
                        <Form.Control type="text" placeholder="Họ và tên" onChange={(event)=> this.isInputChange(event)} name="txtName" required/>
                        <Form.Control.Feedback type="invalid">Vui lòng nhập họ và tên</Form.Control.Feedback>
                    </Form.Group>
                    <Form.Group as={Col} controlId="formGridUsername">
                        <Form.Label>Username</Form.Label>
                        <Form.Control type="text" placeholder="Username" onChange={(event)=> this.isInputChange(event)} name="txtUsername" required/>
                        <Form.Control.Feedback type="invalid">Vui lòng nhập username</Form.Control.Feedback>
                    </Form.Group>
                </Form.Row> 
                <Form.Row>
                    <Form.Group as={Col} controlId="formGridBirthDay">
                        <Form.Label>Năm sinh</Form.Label>
                        <Form.Control type="number" pattern="[1-2\s]{1}[0-9\s]{3}" min="1950" max="2020" maxLength="4" onChange={(event)=> this.isInputChange(event)}  name="txtBirthDay" required placeholder="Năm sinh"/>
                        <Form.Control.Feedback type="invalid">Vui lòng năm sinh đúng định dạng nhỏ hơn 2020 VD:1998,2000,2001,...</Form.Control.Feedback>
                    </Form.Group>
                    <Form.Group as={Col} controlId="formGridPhoneNumber">
                        <Form.Label>Số điện thoại</Form.Label>
                        <Form.Control type="tel" pattern="0[0-9\s]{9}" maxLength="10" placeholder="Số điện thoại" onChange={(event)=> this.isInputChange(event)} name="txtPhoneNumber" required/>
                        <Form.Control.Feedback type="invalid">Vui lòng số điện thoại đúng định dạng VD:0xxxxxxxxx</Form.Control.Feedback>
                    </Form.Group>
                </Form.Row>
                <Form.Row>
                    <Form.Group as={Col} controlId="formGridWage">
                            <Form.Label>Mức lương</Form.Label>
                            <Form.Control type="text" onChange={(event)=> this.isInputChange(event)} placeholder="Mức lương" name="txtWage" required/>
                            <Form.Control.Feedback type="invalid">Vui lòng nhập mức lương</Form.Control.Feedback>
                    </Form.Group>
                    <Form.Group as={Col} controlId="formGridRole">
                            <Form.Label>Chức vụ</Form.Label>
                            <Form.Control as="select" onChange={(event)=> this.isInputChange(event)} name="inputRole" required>
                                <option value=""></option>
                                {role.map((item,key)=>{
                                    return(
                                        <option key={key} value={item.id}>{item.tenquyen}</option>
                                    );
                                })
                                }
                            </Form.Control>
                            <Form.Control.Feedback type="invalid">Vui lòng chọn chức vụ</Form.Control.Feedback>
                    </Form.Group>
                </Form.Row>
                <div className="div-button-submit">
                <Button variant="primary" type="submit" className="submitFormAdd">
                    Thêm nhân viên
                </Button>
                <Button variant="secondary" type="reset" className="buttonReset">
                    Xóa tất cả
                </Button>
                </div>
            </Form>
        );
    }
}
export default FormAddStaff;