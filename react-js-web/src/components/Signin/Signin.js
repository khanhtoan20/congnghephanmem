import axios from "axios";
import React from "react";
import {Form,Button,Tab,Nav,Row,Col,Image,Card} from "react-bootstrap";
import { observer } from "mobx-react";
import ManagePost from "../../view/ManagePost"
import ManageReport from "../../view/ManageReport";
import ManageAccount from "../../view/ManageAccount";
import ManageStaff from "../../view/ManageStaff";
import PostStatisticConfirm from "../PostStatistic/PostStatisticConfirm";
import logo from "../../assets/img/logoSago.png";
import '../../assets/css/styles.css';

import { BrowserRouter as Router,Route} from "react-router-dom";


class Signin extends React.Component{
    constructor(props){
        super(props);
        this.state = {
            txtUsername : null,
            txtPassword : null,
            isLogin :false,
            datalogin : {}
        }
    }
    
    inputChange = (event) => {
        const target = event.target;
        const name = target.name;
        const value = target.value;
        this.setState({
            [name] : value
        })
    }
    Signin = (event) =>{
        event.preventDefault();
        event.target.reset();
        axios.post("http://localhost:8187/api/auth/staff/signin",{
                username : this.state.txtUsername,
                password : this.state.txtPassword
        }).then(response => {
            console.log(response.data);
            if(response.data.statusCode === 1){
                localStorage.setItem('token',response.data.token);
                this.setState({
                    datalogin : response.data
                });
            }
            else {
                alert(response.data.message);
            }
        })
        
    }
    render(){
            if(localStorage.token){
            return (
                <Router>
                    <Route path="/">
                    <div className="App container-fluid">
                        <Tab.Container id="tab-all left-tabs-example" defaultActiveKey="first">
                            <Row className="rowTab">
                                <Col sm={2} className="Tab-main">
                                    <div className="LogoTab">
                                            <Image src={logo} className="logo"></Image>
                                    </div>
                                    <Nav variant="pills" className="flex-column Nav-tab">
                                        <Nav.Item>
                                            <Nav.Link eventKey="first">Thống kê</Nav.Link>
                                        </Nav.Item>
                                        <Nav.Item>
                                            <Nav.Link eventKey="post">Quản lý bài viết</Nav.Link>
                                        </Nav.Item>
                                        <Nav.Item>
                                            <Nav.Link eventKey="report">Quản lý báo cáo</Nav.Link>
                                        </Nav.Item>
                                        <Nav.Item>
                                            <Nav.Link eventKey="account">Quản lý tài khoản</Nav.Link>
                                        </Nav.Item>
                                        <Nav.Item>
                                            <Nav.Link eventKey="staff">Quản lý nhân viên</Nav.Link>
                                        </Nav.Item>
                                    </Nav>
                                </Col>
                                <Col sm={10} className="contentTab">
                                    <Tab.Content>
                                        <Tab.Pane eventKey="first">
                                            <PostStatisticConfirm />
                                        </Tab.Pane>
                                        <Tab.Pane eventKey="post">
                                            <ManagePost />
                                        </Tab.Pane>
                                        <Tab.Pane eventKey="report">
                                            <ManageReport/>
                                        </Tab.Pane>
                                        <Tab.Pane eventKey="account">
                                            <ManageAccount/>
                                        </Tab.Pane>
                                        <Tab.Pane eventKey="staff">
                                            <ManageStaff/>
                                        </Tab.Pane>
                                    </Tab.Content>
                                </Col>
                            </Row>
                        </Tab.Container>
                    </div>
                    </Route>
                </Router>
              );
            }
        return (
                <div className="divLogin">
                    <Card className="cardLogin">
                            <Image src={logo} className="logoLogin"/>
                            <Form onSubmit={(e) => this.Signin(e)}>
                                <Form.Group controlId="formBasicUsername" className="grUsername">
                                    <Form.Label>Username</Form.Label>
                                    <Form.Control type="text" placeholder="Enter Username" onChange={(event) => this.inputChange(event)} name="txtUsername" />
                                </Form.Group>

                                <Form.Group controlId="formBasicPassword" className="grPassword">
                                    <Form.Label>Password</Form.Label>
                                    <Form.Control type="password" placeholder="Password" onChange={(event) => this.inputChange(event)} name="txtPassword"/>
                                </Form.Group>
                                <Button variant="primary" type="submit">
                                    Đăng nhập
                                </Button>
                            </Form>
                    </Card>
                </div>
        );
    }
}
export default observer(Signin);