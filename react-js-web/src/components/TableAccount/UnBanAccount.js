import axios from "axios";
import React from "react";
import {Table,Button} from "react-bootstrap";
const thArrUser = ["ID người dùng","Họ tên","Email","Trạng thái"];

class UnBanAccount extends React.Component{
    constructor(){
        super();
        this.state={
            data : []
        }
        this.unBanUser = this.unBanUser.bind(this);
    }
    componentDidMount(){
        this.fetchDataAccountBanned(); 
    }
    unBanUser(item){
        const token = localStorage.token;
        if(token){
            fetch(`http://localhost:8187/api/staffs/unban/${item.prop.id}`,{
                method : "POST",
                headers :{
                    'Content-Type' : 'application/json',    
                    'Authorization': `Bearer ${token}`
                }
            }).then(res => res.json()).then(response =>{
                alert(response.message);
                this.fetchDataAccountBanned();
            })
        }
        
    }
    fetchDataAccountBanned(){
        const token = localStorage.token;
        if(token) {
            axios.get("http://localhost:8187/api/users/banned").then(response =>{
                this.setState({
                    data : response.data
                })
            })
        }
    }
    render(){
        return(
        <Table>
            <thead>
            <tr>
                {thArrUser.map((prop,key)=> <th key={key}>{prop}</th>)}
            </tr>
            </thead>
            <tbody>
            {this.state.data.map((prop, key) => {
                return (
                    <tr key={key}>
                        <td>{prop.id}</td>
                        <td>{prop.hoTen}</td>
                        <td>{prop.email}</td>
                        <td>Khóa</td>
                        <td>
                            <Button key={key} onClick={()=>this.unBanUser({prop})}>Mở khóa</Button>  
                        </td>
                    </tr>
                    
                );
            })}
            </tbody>
        </Table>           
        );
    }
}
export default UnBanAccount;