import React from "react";
import {Modal} from "react-bootstrap";
import "../../assets/css/styles.css";

export default class PopUp extends React.Component{
    render(){
                return(
                        <Modal show={this.props.show}
                            onHide ={this.props.onHide}
                            size="lg"
                            aria-labelledby="contained-modal-title-vcenter"
                            centered
                            >
                            <Modal.Header closeButton>
                                <Modal.Title id="contained-modal-title-vcenter">
                                    Chi tiết bài viết 
                                </Modal.Title>
                            </Modal.Header>
                            <Modal.Body>
                                <div className="row rowDetail">
                                    <label className="col-xl-4 labelDetail">Giá : </label>
                                     <p className="col-xl-8 contentDetail">{this.props.data.gia}</p>
                                </div>
                                <div className="row rowDetail">
                                    <label className="col-xl-4 labelDetail">Đặt cọc : </label>
                                    <p className="col-xl-8 contentDetail">{this.props.data.datCoc}</p>
                                </div>
                                <div className="row rowDetail">
                                    <label className="col-xl-4 labelDetail">Giá tiền nước : </label>
                                    <p className="col-xl-8 contentDetail">{this.props.data.giaTienNuoc}</p>
                                </div>
                                <div className="row rowDetail">
                                    <label className="col-xl-4 labelDetail">Giá tiền điện : </label>
                                    <p className="col-xl-8 contentDetail">{this.props.data.giaTienDien}</p>
                                </div>
                                <div className="row rowDetail">
                                    <label className="col-xl-4 labelDetail">Giá tiền Wifi: </label>
                                    <p className="col-xl-8 contentDetail">{this.props.data.giaTienWifi}</p>
                                </div>
                                <div className="row rowDetail">
                                    <label className="col-xl-4 labelDetail">Diện tích : </label>
                                    <p className="col-xl-8 contentDetail">{this.props.data.dienTich}</p>
                                </div>
                                <div className="row rowDetail">
                                    <label className="col-xl-4 labelDetail">Sức chứa : </label>
                                    <p className="col-xl-8 contentDetail">{this.props.data.sucChua}</p>
                                </div>
                                <div className="row rowDetail">
                                    <label className="col-xl-4 labelDetail">Địa chỉ : </label>
                                    <p className="col-xl-8 contentDetail">{this.props.data.diaChi}</p>
                                </div>
                            </Modal.Body>
                        </Modal>
                    );
    }
}