import React from "react";

import {Table} from "react-bootstrap";
import axios from "axios";
/*Nội dung bản phê duyệt bài viết*/
const thArrConfirm =["ID bài viết","Tiêu đề","Ngày đăng","Trạng thái"];

class ListPost extends React.Component{

    constructor(props){
        super(props);
        this.state = {
            data : []
        }
    }
    componentDidMount(){
        var token = localStorage.token;
        if(token){
        axios.get("http://localhost:8187/api/posts/staffs",{
                headers: {
                    'Authorization': `Bearer ${token}`
                }
            }).then(response =>{
                this.setState({
                    data : response.data
                })
                }
            )
        }
    }
    render(){
        return (
        <Table>
            <thead>
            <tr>
                {thArrConfirm.map((prop,key)=> <th key={key}>{prop}</th>)}
            </tr>
            </thead>
            <tbody>
            {this.state.data.map((prop, key) => {
                return (
                    <tr key={key}>
                            <td>{prop.id}</td>
                            <td>{prop.tieuDe}</td>    
                            <td>{prop.ngayDang}</td>
                            <td>{prop.trangThai}</td>
                    </tr>
                );
            })}
            </tbody>
        </Table>
        );
    }
}
export default ListPost;