import axios from "axios";
import React from "react";

import {Table,Button,Modal,Form} from "react-bootstrap";
import PopUpPost from "./PopUpPost";
const thArrConfirm =["ID bài viết","Tiêu đề","Trạng thái","Chi tiết","Phê duyệt","Từ chối"];
    
class ConfirmPost extends React.Component{
    constructor(props){
        super(props);
        this.state ={
            setModal: false,
            item : {},
            data : [],
            modalRejected: false,
            txtLiDo: "",
            itemRejected : {}
        };
        this.open = this.open.bind(this);
        this.close  = this.close.bind(this);
        this.openModalRejected= this.openModalRejected.bind(this);
        this.closeModalRejected = this.closeModalRejected.bind(this);
    }
    openModalRejected(item){
        this.setState({
            modalRejected: true,
            itemRejected : item.prop
        });
    }
    closeModalRejected(){
        this.setState({modalRejected : false});
    }
    fetchDataPostPending(){
        const token = localStorage.token;
        if(token){
            axios.get("http://localhost:8187/api/posts/pending",{
                headers: {
                    'Authorization': `Bearer ${token}`
                }
            }).then(response =>{
                this.setState({
                    data : response.data
                })
                }
            )
        }
    }
    componentDidMount(){
        this.fetchDataPostPending();
    }
    close() {
       this.setState({ setModal: false });
    }
    
    open(item) {
       this.setState({ setModal: true,
                        item : item.prop.postDetail
                    });
    }
    clickApprove(item){
        const token = localStorage.token;
        var dataPost = {
            "postID" : item.prop.id,
            "trangThai" : "Approved",
            "liDo" : null
        };
        if(token){
        fetch("http://localhost:8187/api/staffs/approving",{
            method : 'POST',
            headers: {
                'Content-Type' : "application/json",
                'Authorization': `Bearer ${token}`
            },
            body : JSON.stringify(dataPost)
        }
        ).then(res => res.json()).then(response => {
            alert(response.message);
            this.fetchDataPostPending();
        })
        
        }
    }
    inputChange = (event) =>{
        const target = event.target;
        const name = target.name;
        const value = target.value;
        this.setState({
            [name]: value
        });

    }
    clickRejected(item){
        if(this.state.txtLiDo === ""){
            alert("Nhập lí do");
        }else{
            const token = localStorage.token;
            var dataPost={
                "postID" : item.id,
                "trangThai" : "Rejected",
                "liDo" : this.state.txtLiDo
            };
            if(token){
                fetch("http://localhost:8187/api/staffs/approving",{
                    method : "POST",
                    headers : {
                        'Content-Type' : 'application/json',    
                        'Authorization': `Bearer ${token}`
                    },
                    body : JSON.stringify(dataPost)
                }).then(res => res.json()).then(response => {
                    alert(response.message);
                    this.fetchDataPostPending();
                })
                
            }
            this.setState({
                modalRejected : false
            })
        }
    }
    render(){
        return (
            <Table>
                    <thead>
                        <tr>
                            {thArrConfirm.map((prop,key)=> <th key={key}>{prop}</th>)}
                        </tr>
                    </thead>
                    <tbody>
                        {this.state.data.map((prop,key) => {
                                return (
                                    <tr key={key}>               
                                        <td>{prop.id}</td>
                                        <td>{prop.tieuDe}</td>
                                        <td>{prop.trangThai}</td>
                                        <td>
                                            <Button onClick={() =>this.open({prop})} key={key}>Chi tiết</Button>
                                                <PopUpPost show={this.state.setModal}
                                                    onHide = {this.close}   
                                                    data = {this.state.item}                                               
                                                />
                                        </td>
                                        <td>
                                            <Button onClick={() => this.clickApprove({prop})}>
                                                Phê duyệt 
                                            </Button>
                                        </td>
                                        <td>
                                            <Button onClick={() => this.openModalRejected({prop})}>
                                                Từ chối
                                            </Button>
                                                <Modal show={this.state.modalRejected}
                                                        onHide={this.closeModalRejected}
                                                        size="lg"
                                                        aria-labelledby="contained-modal-title-vcenter"
                                                        centered
                                                >
                                                    <Modal.Header closeButton>
                                                        <Modal.Title id="contained-modal-title-vcenter">
                                                            Lí do từ chối bài viết
                                                        </Modal.Title>
                                                    </Modal.Header>
                                                    <Modal.Body>
                                                    <Form.Group>
                                                        <Form.Label>Lí do</Form.Label>
                                                        <Form.Control placeholder="Nhập lí do từ chối" name="txtLiDo" onChange={(event)=> this.inputChange(event)}/>
                                                    </Form.Group>
                                                    </Modal.Body>
                                                    <Modal.Footer>
                                                        <Button onClick={() =>this.clickRejected(this.state.itemRejected)}>
                                                            Xác nhận
                                                        </Button>
                                                    </Modal.Footer>
                                                </Modal>
                                        </td>
                                    </tr>
                                    );
                        })}
                    </tbody>
            </Table>                    
        );
    }
}
export default ConfirmPost;