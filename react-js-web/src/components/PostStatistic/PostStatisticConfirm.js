import React from "react";
import axios from "axios";
import {Card, Table} from "react-bootstrap";
import Header from "../../components/Header/Header";
import {HorizontalBar} from "react-chartjs-2";
import "../../assets/css/styles.css";
const thTable = ["id","Tiêu đề","Lượt xem"];
export default class PostStatisticConfirm extends React.Component{
    constructor(props){
        super(props);
        this.state ={
            dataStatisticPost : [],
            dataView : []
        };
       this.setDataChart = this.setDataChart.bind(this);
       this.fetchDataStatisticPost = this.fetchDataStatisticPost.bind(this);
    }
    componentDidMount(){
        this.fetchDataStatisticPost();
        this.fetchDataView();
        setInterval(this.fetchDataStatisticPost,30000);
    }
    fetchDataStatisticPost(){
        axios.get("http://localhost:8187/api/posts/statistic").then(response =>{
            this.setDataChart(response.data);
        });
    }
    setDataChart(params){
        this.setState({
            dataStatisticPost : params
        })
    }
    fetchDataView(){
        const token = localStorage.token;
        if(token){
            fetch("http://localhost:8187/api/posts/topview/10",{
                method :"POST",
                headers :{
                    'Authorization': `Bearer ${token}`
                }
            }).then(res => res.json()).then(response=>{
                this.setState({
                    dataView : response
                })
            })
        }
    }
    render(){
        return (
            <div className="container-fluid ">
                <div className="row rowContent">
                    <div className="mainContent col-lg-12">
                        <Header title="Thống kế"/>
                        <Card className="cardPostStatistic">
                            <HorizontalBar
                                className="barPost"
                                data={{
                                labels: [
                                    "Đã phê duyệt",
                                    "Chờ phê duyệt",
                                    "Đã từ chối",
                                    "Đã xóa"
                                ],
                                datasets: [
                                    {
                                    barThickness : 30,
                                    borderWidth: 2.5,
                                    borderColor: "#000",
                                    label: "Số lượng",
                                    backgroundColor: [
                                        "rgba(8, 255, 62, 0.4)",
                                        "rgba(224, 255, 7, 0.4)",
                                        "rgba(255, 0, 0, 0.4)",
                                        "rgba(200, 200, 200,0.4)",
                                    ],
                                    data: this.state.dataStatisticPost
                                    }
                                ]
                                }}
                                options={{
                                scales: {
                                    xAxes: [{
                                        ticks: {
                                            beginAtZero: true
                                        }
                                    }],
                                },
                                legend: { display: false},
                                title: {
                                    fontSize:25,
                                    display: true,
                                    text: "Thống kế số lượng bài viết "
                                }
                                }}
                            />
                            <div className="divTopView">
                                <h4 className="titleTopView">Bài viết có lượt xem cao</h4>
                                <Table className="tableTopView">
                                <thead>
                                    <tr>
                                        {thTable.map((prop,key)=> <th key={key}>{prop}</th>)}
                                    </tr>
                                    </thead>
                                    <tbody>
                                    {this.state.dataView.map((prop,key)=>{
                                            return(
                                                <tr key={key}>
                                                    <td>{prop.id}</td>
                                                    <td>{prop.tieuDe}</td>
                                                    <td>{prop.luotXem}</td>
                                                </tr>
                                            );
                                        })
                                    }
                                    </tbody>
                                </Table>
                            </div>                            
                        </Card>
                    </div>
                    
                </div>
            </div>
        );
    }
}