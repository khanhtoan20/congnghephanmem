import axios from "axios";
import React from "react";
import {Table,Button} from "react-bootstrap";

/*Nội dung table report post*/
const thArrReportPost = ["ID bài viết","Lý do báo cáo","Id bài viết","Trạng thái","Xác Nhận"];

class ReportPost extends React.Component{
    constructor(){
        super();
        this.state={
            data : []
        }
    }
    fetchDataReportPending(){
        const token = localStorage.token;
        if(token){
            axios.get("http://localhost:8187/api/reports/pending",{
                headers :{
                    'Authorization': `Bearer ${token}`
                }
            }).then(response => {
                this.setState({
                    data : response.data
                })
                
            })
        }
    }
    componentDidMount(){
        this.fetchDataReportPending();
    }
    clickAcceptReport(item){
        const token = localStorage.token;
        
        if(token){
            fetch(`http://localhost:8187/api/staffs/handling/${item.prop.id}`,{
                method : 'POST',
                headers :{
                    "Content-Type" : "application/json",
                    'Authorization': `Bearer ${token}`
                }
            }).then(res => res.json()).then(response => {
                alert(response.message);
                this.fetchDataReportPending();
            })
           
        }
    }
    render(){
        return(
        <Table>
            <thead>
            <tr>
                {thArrReportPost.map((prop,key)=> <th key={key}>{prop}</th>)}
            </tr>
            </thead>
            <tbody>
            {this.state.data.map((prop, key) => {
                return (
                    <tr key={key}>
                        <td>{prop.id}</td>
                        <td>{prop.reason}</td>
                        <td>{prop.postId}</td>
                        <td>{prop.status}</td>
                        <td><Button onClick={() => this.clickAcceptReport({prop})}>Xác nhận</Button></td>
                    </tr>
                );
            })}
            </tbody>
        </Table>
        );
    }
}
export default ReportPost;