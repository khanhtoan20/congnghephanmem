import axios from "axios";
import React from "react";
import {Table}  from "react-bootstrap";

/*Nội dung table report post*/
const thArrReportPost = ["ID bài viết","Lý do báo cáo","Xác Nhận"];


class ListPost extends React.Component{
    constructor(){
        super();
        this.state ={
            data : []
        }
    }
    componentDidMount(){
      const token = localStorage.token;
      if(token){
          axios.get("http://localhost:8187/api/reports",{
              headers:{
                  'Authorization': `Bearer ${token}`
              }
          }).then(response =>{
              this.setState({
                  data : response.data
              })
          })
      }
    }
    render(){
        return(
        <Table>
            <thead>
            <tr>
                {thArrReportPost.map((prop,key)=> <th key={key}>{prop}</th>)}
            </tr>
            </thead>
            <tbody>
            {this.state.data.map((prop,key) =>{
                return(
                    <tr key={key}>
                        <td>{prop.id}</td>
                        <td>{prop.reason}</td>
                        <td>{prop.status}</td>
                    </tr>
                );
            })

            }
            
            </tbody>
        </Table>

        );
    }
}
export default ListPost;