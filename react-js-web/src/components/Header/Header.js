import React from "react";
import {Image, Button, Card} from "react-bootstrap";
import "../../assets/css/styles.css";
import logo from "../../assets/img/admin.png";
class Header extends React.Component{
    constructor(){
        super();
        this.logOut = this.logOut.bind(this);
    }
    logOut(){
        localStorage.setItem('token','');
    }
    render() {
        return(
            <Card className="cardHeader">
                <div className="header d-flex justify-content-between">
                    <h4>{this.props.title}</h4>
                        <div className="d-flex justify-content-around logOutAdmin">
                            <div className="iconAdmin">
                                <Image src={logo}/>
                            </div>
                                <Button onClick={() => this.logOut()} href="./">Đăng xuất</Button>
                        </div>
                </div>
            </Card>
        );
    }
}
export default Header;