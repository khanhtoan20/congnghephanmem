﻿-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Máy chủ: 127.0.0.1
-- Thời gian đã tạo: Th10 20, 2020 lúc 07:15 AM
-- Phiên bản máy phục vụ: 10.4.14-MariaDB
-- Phiên bản PHP: 7.4.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Cơ sở dữ liệu: `sagostay`
--

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `facility`
--

CREATE TABLE `facility` (
  `matienich` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `url` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ten_tien_ich` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `facility`
--

INSERT INTO `facility` (`matienich`, `url`, `ten_tien_ich`) VALUES
('ml', 'url', 'Máy lạnh'),
('nvsr', 'url', 'Nhà vệ sinh riêng'),
('bdx', 'url', 'Bãi để xe'),
('wf', 'url', 'Wifi');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `hibernate_sequence`
--

CREATE TABLE `hibernate_sequence` (
  `next_val` bigint(20) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `hibernate_sequence`
--

INSERT INTO `hibernate_sequence` (`next_val`) VALUES
(15);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `image`
--

CREATE TABLE `image` (
  `id` bigint(20) NOT NULL,
  `url` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `post_id` bigint(20) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `notification`
--

CREATE TABLE `notification` (
  `id` bigint(20) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `content` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `status` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `type` bit(1) DEFAULT NULL,
  `user_id` bigint(20) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `notification`
--

INSERT INTO `notification` (`id`, `created_at`, `updated_at`, `content`, `status`, `type`, `user_id`) VALUES
(1, '2020-11-13 09:44:18', '2020-11-13 09:44:18', '<b>ID :11</b><br>Bài đăng của bạn đang được phê duyệt', '0', b'1', 8),
(2, '2020-11-20 03:49:49', '2020-11-20 03:49:49', '<b>ID :11</b><br>Bài đăng của bạn vùa cập nhật và đang được phê duyệt', '0', b'1', 8),
(3, '2020-11-20 04:24:16', '2020-11-20 04:24:16', '<b>ID :1</b><br>Bài đăng của bạn vùa cập nhật và đang được phê duyệt', '0', b'1', 1),
(4, '2020-11-20 04:27:11', '2020-11-20 04:27:11', '<b>ID :2</b><br>Bài đăng của bạn vùa cập nhật và đang được phê duyệt', '0', b'1', 1),
(5, '2020-11-20 04:30:21', '2020-11-20 04:30:21', '<b>ID :3</b><br>Bài đăng của bạn vùa cập nhật và đang được phê duyệt', '0', b'1', 1);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `post`
--

CREATE TABLE `post` (
  `id` bigint(20) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `luotxem` bigint(20) DEFAULT 0,
  `status` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `tieude` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `user_id` bigint(20) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `post`
--

INSERT INTO `post` (`id`, `created_at`, `updated_at`, `luotxem`, `status`, `tieude`, `user_id`) VALUES
(1, '2020-11-08 13:36:46', '2020-11-20 04:24:15', 1, 'Available', 'Căn hộ dịch vụ Tôn Thất Tùng, Quận 1', 1),
(2, '2020-11-08 13:37:17', '2020-11-20 04:27:11', 1, 'Available', 'Kí túc xá/Homestay Hoàng Văn Thụ, Quận Tân Bình', 1),
(3, '2020-11-08 13:43:19', '2020-11-20 04:29:57', 0, 'Available', 'CHDV Sân Bay TSN. Full Nội Thất Mức Giá Thuê Cực Rẻ', 1),
(4, '2020-11-08 13:49:51', '2020-11-08 13:49:51', 0, 'Available', 'Em trọ cực múp', 8),
(5, '2020-11-08 16:05:49', '2020-11-08 16:05:49', 0, 'Available', 'Em trọ cực múp', 8),
(6, '2020-11-09 04:03:02', '2020-11-09 04:03:02', 0, 'Available', 'Em trọ cực múp part 2', 8),
(7, '2020-11-09 04:08:20', '2020-11-09 04:08:20', 0, 'Available', 'Em trọ cực múp part 2', 8),
(9, '2020-11-13 07:59:51', '2020-11-20 03:58:51', 1, 'Available', 'Kí túc xá/Homestay Hoàng Văn Thụ, Quận Tân Bình', 8),
(10, '2020-11-13 09:04:01', '2020-11-20 03:58:54', 1,'Available', 'TIEU ANH QUOC', 8),
(11, '2020-11-13 09:44:17', '2020-11-20 04:33:39', 6, 'Available', 'Kí túc xá/Homestay Bình Quới, Quận Bình Thạnh', 8);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `post_detail`
--

CREATE TABLE `post_detail` (
  `id` bigint(20) NOT NULL,
  `chuoitienich` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `datcoc` bigint(20) DEFAULT NULL,
  `diachi` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `dientich` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `gia` bigint(20) DEFAULT NULL,
  `giatiendien` bigint(20) DEFAULT NULL,
  `giatiennuoc` bigint(20) DEFAULT NULL,
  `giatienwifi` bigint(20) DEFAULT NULL,
  `mota` longtext COLLATE utf8_unicode_ci DEFAULT NULL,
  `succhua` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `trangthaiphong` bit(1) DEFAULT NULL,
  `post_id` bigint(20) DEFAULT NULL,
  `lienlac` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `post_detail`
--

INSERT INTO `post_detail` (`id`, `chuoitienich`, `datcoc`, `diachi`, `dientich`, `gia`, `giatiendien`, `giatiennuoc`, `giatienwifi`, `mota`, `succhua`, `trangthaiphong`, `post_id`, `lienlac`) VALUES
(2, 'tv-ad', 1500000, '214/10 Hoàng Văn Thụ, Phường 4, Quận Tân Bình, Hồ Chí Minh', '40', 1500000, 1500000, 1500000, 1500000, 'Dinh cao nhat sai gon', '5-10', b'0', 9, NULL),
(3, 'tv-ad', 1500000, '214/10 Hoàng Văn Thụ, Phường 4, Quận Tân Bình, Hồ Chí Minh', '40', 1500000, 1500000, 1500000, 1500000, 'Dinh cao nhat sai gon', '5-10', b'0', 10, NULL),
(4, 'tv-ad', 1500000, '117/2b Bình Quới, Phường 27, Quận Bình Thạnh, Hồ Chí Minh', '40', 1500000, 1500000, 1500000, 1500000, 'KTX_tân_bình  - Giờ giấc tự do - Phòng chỉ 4 người\n---------------------\n Ktx tân bình tọa lạc tại vị trí trung tâm thuận tiện đi lại các quận: Q1, Q3, Phú Nhuận, Gò Vấp\n  Cạnh các khu vực:\n CV Hoàng Văn Thụ, Lotte Mart Cộng Hoà, ADORA, AUCHAN, Sân bay Tân Sơn Nhất, ...\n Trường Saigontouris, CĐ Lý Tự Trọng, ĐH Văn Hiến, Đh Bách Khoa, ĐH Kinh Tế, CĐ Hàng Không\n---------------------\n Phòng rộng thoáng mát, có máy lạnh, có ban công, giường, rèm, nệm, kệ sách, tủ cá nhân.\n Nhà để xe có camera 24/24, \n Phòng giặt: Mát giặt, xào phơi đồ.\n Khu bếp: Tủ lạnh, bếp điện, bàn ăn, kệ chén,...\n---------------------\n Gía chỉ 1tr5/người/tháng\n----------------------\nLiên hệ trực tiếp ktx tân bình để nhận thêm ưu đãi\n Zalo: 0832.232.232\n 214/10 Hoàng Văn Thụ, Phường 4, Q. Tân Bình. (Đối diện ADORA)', '5-10', b'0', 11, NULL),
(5, 'tv-ad', 1500000, '25/40 B Tôn Thất Tùng, Phường Phạm Ngũ Lão, Quận 1, Hồ Chí Minh', '40', 1500000, 1500000, 1500000, 1500000, 'An ninh, trật tự, sạch sẽ và yên tĩnh. Mặt tiền đường ô tô, gần nhiều chợ, siêu thị mini, thuận tiện đến các quận trung tâm!!', '5-10', b'0', 1, '0932735616'),
(6, 'ml-nvsr-bdx-wf', 1500000, '214/10 Hoàng Văn Thụ, Phường 4, Quận Tân Bình, Hồ Chí Minh', '40', 1500000, 1500000, 1500000, 1500000, 'KTX_tân_bình  - Giờ giấc tự do - Phòng chỉ 4 người\n---------------------\n Ktx tân bình tọa lạc tại vị trí trung tâm thuận tiện đi lại các quận: Q1, Q3, Phú Nhuận, Gò Vấp\n  Cạnh các khu vực:\n CV Hoàng Văn Thụ, Lotte Mart Cộng Hoà, ADORA, AUCHAN, Sân bay Tân Sơn Nhất, ...\n Trường Saigontouris, CĐ Lý Tự Trọng, ĐH Văn Hiến, Đh Bách Khoa, ĐH Kinh Tế, CĐ Hàng Không\n---------------------\n Phòng rộng thoáng mát, có máy lạnh, có ban công, giường, rèm, nệm, kệ sách, tủ cá nhân.\n Nhà để xe có camera 24/24, \n Phòng giặt: Mát giặt, xào phơi đồ.\n Khu bếp: Tủ lạnh, bếp điện, bàn ăn, kệ chén,...\n---------------------\n Gía chỉ 1tr5/người/tháng\n----------------------\nLiên hệ trực tiếp ktx tân bình để nhận thêm ưu đãi\n Zalo: 0832.232.232\n 214/10 Hoàng Văn Thụ, Phường 4, Q. Tân Bình. (Đối diện ADORA)', '5-10', b'0', 2, '0832232232'),
(7, 'nvsr-bdx-wf', 5500000, '2R Trường Sơn, Phường 2, Quận Tân Bình, Hồ Chí Minh', '40', 1500000, 1500000, 100000, 100000, 'Địa chỉ: 2R Trường Sơn, P2, Tân Bình\nMặt tiền, khu sân bay cao cấp.\nNgay lá phổi Xanh CV Hoàng Văn Thụ\n\n Free phí dịch vụ: Máy giặt + Sấy, rác, vệ sinh nhà hàng tuần, hút bụi tận phòng, để xe máy...\n\n Nội thất Full cao cấp: Sofa, giường tủ quần áo, tủ kệ bếp, bàn ăn, Smart tivi 40”, Máy lạnh, tủ lạnh, Bếp hồng ngoại cao cấp...\n\n Giá thuê: \n- CHDV 28m2, giá thuê 6.5 triệu -> Ưu đãi tháng đầu tiên 5.5 Triệu/ tháng ( Cửa sổ, logia)\n\n Quản lý chuyên nghiệp theo hệ thống, luôn có người bảo trì, vận hành thân thiện\n\n Xem phòng: 097.379.7286 Mr. Hùng\n)', '5-10', b'0', 3, '0973797286'),
(8, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, b'0', 4, NULL),
(9, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, b'0', 5, NULL),
(10, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, b'0', 6, NULL),
(11, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, b'0', 7, NULL);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `report`
--

CREATE TABLE `report` (
  `id` bigint(20) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `reason` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `type` int(11) DEFAULT NULL,
  `post_id` bigint(20) DEFAULT NULL,
  `user_id` bigint(20) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `role`
--

CREATE TABLE `role` (
  `id` bigint(20) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `tenquyen` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `role`
--

INSERT INTO `role` (`id`, `created_at`, `updated_at`, `tenquyen`) VALUES
(1, '2020-11-06 14:36:36', '2020-11-06 14:36:36', 'Hệ thống'),
(2, '2020-11-06 14:36:36', '2020-11-06 14:36:36', 'Quản lý nhân viên'),
(3, '2020-11-06 14:36:36', '2020-11-06 14:36:36', 'Quản lý bài đăng'),
(4, '2020-11-06 14:36:36', '2020-11-06 14:36:36', 'Chăm sóc khách hàng'),
(5, '2020-11-06 14:36:36', '2020-11-06 14:36:36', 'Quản lý tài khoản');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `staff`
--

CREATE TABLE `staff` (
  `id` bigint(20) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `hoten` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `luong` bigint(20) DEFAULT NULL,
  `namsinh` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `sodienthoai` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `username` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `role_id` bigint(20) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `staff`
--

INSERT INTO `staff` (`id`, `created_at`, `updated_at`, `hoten`, `luong`, `namsinh`, `password`, `sodienthoai`, `username`, `role_id`) VALUES
(0, '2020-11-08 13:33:53', '2020-11-08 13:33:53', 'Hệ Thống', NULL, '', '$2a$10$lxebrPEsH1w3MXdJpH.KNeAkg5twQSsSk5EuVowrsadVv3Kk4Vmx2', '', 'system', 1);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `staff_post_history`
--

CREATE TABLE `staff_post_history` (
  `id` bigint(20) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `lido` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `trangthai` varchar(255) COLLATE utf8_unicode_ci DEFAULT 'Pending',
  `post_id` bigint(20) NOT NULL,
  `staff_id` bigint(20) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `staff_post_history`
--

INSERT INTO `staff_post_history` (`id`, `created_at`, `updated_at`, `lido`, `trangthai`, `post_id`, `staff_id`) VALUES
(1, '2020-11-08 13:36:46', '2020-11-08 13:36:46', NULL, 'Pending', 1, 0),
(2, '2020-11-08 13:37:17', '2020-11-08 13:37:17', NULL, 'Pending', 2, 0),
(3, '2020-11-08 13:43:19', '2020-11-08 13:43:19', NULL, 'Pending', 3, 0),
(4, '2020-11-08 13:49:51', '2020-11-08 13:49:51', NULL, 'Pending', 4, 0),
(5, '2020-11-08 16:05:49', '2020-11-08 16:05:49', NULL, 'Pending', 5, 0),
(6, '2020-11-09 04:03:03', '2020-11-09 04:03:03', NULL, 'Pending', 6, 0),
(7, '2020-11-09 04:08:20', '2020-11-09 04:08:20', NULL, 'Pending', 7, 0),
(9, '2020-11-13 07:59:51', '2020-11-13 07:59:51', NULL, 'Pending', 9, 0),
(10, '2020-11-13 09:04:01', '2020-11-13 09:04:01', NULL, 'Pending', 10, 0),
(11, '2020-11-13 09:44:18', '2020-11-13 09:44:18', NULL, 'Pending', 11, 0),
(12, '2020-11-18 10:22:49', '2020-11-18 10:22:49', '', 'Approved', 11, 0),
(13, '2020-11-20 03:49:49', '2020-11-20 03:49:49', NULL, 'Pending', 11, 0),
(14, '2020-11-20 03:56:35', '2020-11-20 03:56:35', '', 'Approved', 11, 0),
(15, '2020-11-20 04:24:16', '2020-11-20 04:24:16', NULL, 'Pending', 1, 0),
(16, '2020-11-20 04:27:11', '2020-11-20 04:27:11', NULL, 'Pending', 2, 0),
(17, '2020-11-20 04:30:21', '2020-11-20 04:30:21', NULL, 'Pending', 3, 0);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `users`
--

CREATE TABLE `users` (
  `id` bigint(20) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `hoten` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `is_verified_email` bit(1) DEFAULT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `sodienthoai` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `is_locked` bit(1) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `users`
--

INSERT INTO `users` (`id`, `created_at`, `updated_at`, `email`, `hoten`, `is_verified_email`, `password`, `sodienthoai`, `is_locked`) VALUES
(1, '2020-10-30 08:40:01', '2020-10-30 08:46:03', 'phancongha24@gmail.com', 'Phan Công Hà', b'1', '$2a$10$GwOt6PV1WQfoVz4qbdM2DO.Femt7mwE0evat.YAfGaiEd/95gbnT.', '0929315514', b'0'),
(2, '2020-10-30 08:55:51', '2020-10-30 08:57:18', 'aukhanhtoan@gmail.com', 'Âu Khánh Toàn', b'1', '$2a$10$CBcj/5Toc5KPVUl9tlcf/eLWcXfeByh5ep4.CrrajrRU4XRHbEQFu', '0929315514', b'0'),
(3, '2020-11-06 05:08:29', '2020-11-06 05:08:29', 'phancongha25@gmail.com', 'Phan Công Sơn', b'0', '$2a$10$TaOTR4Be.r3cAPi2QtLNxu4qSRBxW/xkiiIQXKn0Kce9xnM9N.mmK', '0929315514', b'0'),
(4, '2020-11-06 06:41:03', '2020-11-06 06:41:03', 'phancongha26@gmail.com', 'Phan Công Sơn', b'0', '$2a$10$5neGSn7Sbru07NpAtXwfPOzN1uS5NAtmHh/aFfucv1w6YMa.J2jN6', '0929315514', b'0'),
(5, '2020-11-06 06:42:12', '2020-11-06 06:42:12', 'phancongha27@gmail.com', 'Phan Công Sơn', b'0', '$2a$10$tj9tWWxUbhOtgoZbga.b8er4Zhs6adqi.P0.QpRp1k.gFwXIFLc5G', '0929315514', b'0'),
(6, '2020-11-06 06:56:54', '2020-11-06 06:56:54', 'phancongha28@gmail.com', 'Phan Công Sơn', b'0', '$2a$10$k1Y9RR3U7yy2OYoeRqo0HuWCH2XyFmSdsfO3jXuiQCJQaGt/3nvlm', '0929315514', b'0'),
(7, '2020-11-06 07:03:38', '2020-11-06 07:03:38', 'phancongha29@gmail.com', 'Phan Công Sơn', b'0', '$2a$10$jB8/DHxgR0yvbDbdIRXLnODjbUDxZ4K.7h.8UOOwAwJqRHqqKfXVS', '0929315514', b'0'),
(8, '2020-11-06 07:04:47', '2020-11-08 13:46:24', 'phancongha30@gmail.com', 'Phan Công Sơn', b'1', '$2a$10$06zc61MNBvyF60thSH4ugu.wfomJkQhB4RU/PtLbjzUrsvwaO4jhC', '0929315514', b'0');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `verification_token`
--

CREATE TABLE `verification_token` (
  `id` bigint(20) NOT NULL,
  `expired_date` datetime DEFAULT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `user_id` bigint(20) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `verification_token`
--

INSERT INTO `verification_token` (`id`, `expired_date`, `token`, `user_id`) VALUES
(8, NULL, '76a42daa-d9e7-4aee-b078-ca8b2c60c326', 3),
(9, NULL, '420c8d02-cdbd-4e22-a447-c4270e42a63f', 4),
(10, NULL, 'd42fb1c3-da52-450f-bfe4-a2cc9fabcc7a', 5),
(11, NULL, '69fa8301-99e1-4c13-b75d-264072f51b54', 6),
(12, NULL, '3af33794-16d1-4c65-b35a-4ae19a6948c4', 7);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `wishlist`
--

CREATE TABLE `wishlist` (
  `user_id` bigint(20) NOT NULL,
  `post_id` bigint(20) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Chỉ mục cho các bảng đã đổ
--

--
-- Chỉ mục cho bảng `facility`
--
ALTER TABLE `facility`
  ADD PRIMARY KEY (`matienich`);

--
-- Chỉ mục cho bảng `image`
--
ALTER TABLE `image`
  ADD PRIMARY KEY (`id`),
  ADD KEY `FKe2l07hc93u2bbjnl80meu3rn4` (`post_id`);

--
-- Chỉ mục cho bảng `notification`
--
ALTER TABLE `notification`
  ADD PRIMARY KEY (`id`),
  ADD KEY `FKnk4ftb5am9ubmkv1661h15ds9` (`user_id`);

--
-- Chỉ mục cho bảng `post`
--
ALTER TABLE `post`
  ADD PRIMARY KEY (`id`),
  ADD KEY `FK7ky67sgi7k0ayf22652f7763r` (`user_id`);

--
-- Chỉ mục cho bảng `post_detail`
--
ALTER TABLE `post_detail`
  ADD PRIMARY KEY (`id`),
  ADD KEY `FK46mm0e5earch2ws3ffhl533aa` (`post_id`);

--
-- Chỉ mục cho bảng `report`
--
ALTER TABLE `report`
  ADD PRIMARY KEY (`id`),
  ADD KEY `FKnuqod1y014fp5bmqjeoffcgqy` (`post_id`),
  ADD KEY `FKq50wsn94sc3mi90gtidk0k34a` (`user_id`);

--
-- Chỉ mục cho bảng `role`
--
ALTER TABLE `role`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `staff`
--
ALTER TABLE `staff`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `UKn5ib031h2ipdsj507srabt3kf` (`username`),
  ADD KEY `FK5bbdfuitxii0b63v2v3f0r22x` (`role_id`);

--
-- Chỉ mục cho bảng `staff_post_history`
--
ALTER TABLE `staff_post_history`
  ADD PRIMARY KEY (`id`),
  ADD KEY `FK55dyfillgau667t7tyj4t9w76` (`post_id`),
  ADD KEY `FK3niuiihjg3k65df8wm79rd18r` (`staff_id`);

--
-- Chỉ mục cho bảng `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `UK6dotkott2kjsp8vw4d0m25fb7` (`email`);

--
-- Chỉ mục cho bảng `verification_token`
--
ALTER TABLE `verification_token`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `UK_p678btf3r9yu6u8aevyb4ff0m` (`token`),
  ADD KEY `FK3asw9wnv76uxu3kr1ekq4i1ld` (`user_id`);

--
-- Chỉ mục cho bảng `wishlist`
--
ALTER TABLE `wishlist`
  ADD PRIMARY KEY (`user_id`),
  ADD KEY `FKceor61aistecmo62mqb8b8dgd` (`post_id`);

--
-- AUTO_INCREMENT cho các bảng đã đổ
--

--
-- AUTO_INCREMENT cho bảng `image`
--
ALTER TABLE `image`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT cho bảng `notification`
--
ALTER TABLE `notification`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT cho bảng `post`
--
ALTER TABLE `post`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT cho bảng `post_detail`
--
ALTER TABLE `post_detail`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT cho bảng `report`
--
ALTER TABLE `report`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT cho bảng `role`
--
ALTER TABLE `role`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT cho bảng `staff`
--
ALTER TABLE `staff`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT cho bảng `staff_post_history`
--
ALTER TABLE `staff_post_history`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT cho bảng `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
